<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2023 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Shortcoder {

	public $baseDir;
	public $alphabet = 'abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789';
	public $defaultLength = 3;


	public function __construct( $baseDir ){
		$this->baseDir = $baseDir;
		if ( ! file_exists( $baseDir ) ){
			mkdir( $baseDir, 0755, TRUE );
		}
	}


	public function create( $value, $module, $length = NULL ){
		$length = $length ?? $this->defaultLength;
		$lastPos = strlen( $this->alphabet ) - 1;
		$trials = 0;
		$dir = "{$this->baseDir}/{$module}";
		if ( ! file_exists( $dir ) ){
			mkdir( $dir, 0755 );
		}
		for ( $trials = 0; $trials < 10; $trials++ ){
			$code = '';
			do {
				for ( $i = 0; $i < $length; $i++ ) {
					$code .= substr( $this->alphabet, rand( 0, $lastPos ), 1 );
				}
				$file = "{$dir}/{$code}";
			} while ( $exists = file_exists( $file ) );
		}
		if ( $exists ) {
			return FALSE; // all tried codes are in use already
		}
		if ( ! file_put_contents( $file, $value ) ){
			return FALSE; // could not write file
		}
		return $code;
	}


	public function resolve( $code, $module ){
		$code = $this->clean_code( $code );
		$file = "{$this->baseDir}/{$module}/{$code}";
		if ( ! file_exists( $file ) ){
			return FALSE; // code is undefined
		}
		return file_get_contents( $file );
	}


	public function list( $module ){
		$dir = "{$this->baseDir}/{$module}";
		if ( ! file_exists( $dir ) ){
			return [];
		}
		$list = array_filter( scandir( $dir ), function( $d ){ return $d[0] != '.'; } );
		return $list;
	}


	public function delete( $code, $module ){
		$code = $this->clean_code( $code );
		$file = "{$this->baseDir}/{$module}/{$code}";
		if ( ! file_exists( $file ) ){
			return FALSE; // code is undefined
		}
		return unlink( $file );
	}


	public function clean_code( $code ){
		if ( $code[0] == '#' ){
			$code = substr( $code, 1 );
		}
		list ( $code ) = explode( '.', $code );
		return $code;
	}


	public function convert_to_digits( $code ){
		$fromBase = str_split( $this->alphabet, 1 );
		$fromLen = strlen( $this->alphabet );
		$codeArr = str_split( $code, 1 );
		$numberLen = strlen( $code );
		$retval = 0;
		for ( $i = 1; $i <= $numberLen; $i++ ){
			$retval = bcadd( $retval, bcmul( array_search( $codeArr[ $i - 1 ], $fromBase ),
			bcpow( $fromLen, $numberLen - $i ) ) );
		}
		return $retval;
	}


	public function convert_from_digits( $code ){
		$base10 = (int) $code;
		$toBase = str_split( $this->alphabet, 1 );
		$toLen = strlen( $this->alphabet );
		if ( $base10 < $toLen ){
			return $toBase[ $base10 ];
		}
		$retval='';
		while( $base10 != '0' ){
			$retval = $toBase[ bcmod( $base10, $toLen ) ] . $retval;
			$base10 = bcdiv( $base10, $toLen, 0 );
		}
		return $retval;
	}
}

// end of file lib/shortcoder.class.php

//$coder = new ParTCP_Shortcoder( '/' );
//echo $coder->convert_to_digits('abc');
//echo $coder->convert_from_digits('58');

