<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Counter {

	public $dataDir;
	public $baseDir;


	public function __construct( $dataDir ){
		$dataDir = rtrim( $dataDir, '/' );
		$this->dataDir = $dataDir;
		$this->baseDir = $dataDir;
	}


	public function set_base_dir( $baseDir ){
		$baseDir = trim( $baseDir, '/' );
		$this->baseDir = "{$this->dataDir}/{$baseDir}";
	}


	public function increment( $counterId ){
		$file = "{$this->baseDir}/counters/{$counterId}";
		$dir = dirname( $file );
		if ( ! file_exists( $dir ) ){
			mkdir( $dir, 0755, TRUE );
		}
		return file_put_contents( $file, '.', FILE_APPEND | LOCK_EX );
	}


	public function get_value( $counterId ){
		$file = "{$this->baseDir}/counters/{$counterId}";
		return file_exists( $file ) ? filesize( $file ) : 0;
	}


	public function delete( $counterId ){
		$file = "{$this->baseDir}/counters/{$counterId}";
		return ! file_exists( $file ) || unlink( $file );
	}

}

// end of file lib/counter.class.php

