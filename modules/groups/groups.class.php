<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Groups {
	
	public $fileSystem;
	public $options;


	public function __construct( $fileSystem, $options = [] ){
		$this->fileSystem = $fileSystem;
		$this->options = $options;
	}


	public function get_dir( $id ){
		$items = explode( '/', $id );
		return 'groups/' . implode( '/groups/', $items );
	}


	public function get_list( $id = '' ){
		$dir = ( $id ? $this->get_dir( $id ) : '' ) . '/groups';
		$listing = $this->fileSystem->get_listing( $dir );
		if ( ! $listing ){
			return [];
		}
		$groups = [];
		foreach ( $listing as $item ){
			$groupId = ( $id ? "{$id}/" : '' ) . $item;
			if ( $data = $this->get_data( $groupId ) ){
				$groups[] = $data;
			}
		}
		return $groups;
	}


	public function get_list_recursive( $id = '' ){
		$idList = $this->get_id_list_recursive( $id );
		$groups = [];
		foreach ( $idList as $groupId ){
			if ( $data = $this->get_data( $groupId ) ){
				$groups[] = $data;
			}
		}
		return $groups;
	}


	public function get_id_list_recursive( $id = '' ){
		$dir = $id ? $this->get_dir( $id ) : '';
		return $this->get_subgroups( $dir, $id ? "{$id}/" : '' );
	}


	private function get_subgroups( $groupDir, $prefix = '' ){
		$dir = "{$groupDir}/groups";
		$subgroups = [];
		$listing = $this->fileSystem->get_listing( $dir );
		foreach ( $listing as $item ){
			$subgroups[] = "{$prefix}{$item}";
			$subgroups = array_merge( $subgroups,
				$this->get_subgroups( "{$dir}/{$item}", "{$prefix}{$item}/" )
			);
		}
		return $subgroups;
	}


	public function get_data( $id ){
		$dir = $this->get_dir( $id );
		$data = $this->fileSystem->get_recent_contents( $dir );
		if ( ! $data ){
			return FALSE;
		}
		$receipt = yaml_parse( $data );
		if ( empty( $receipt['Group-Data'] ) ){
			return FALSE;
		}
		return $receipt['Group-Data'];
	}


	public function is_member( $participantId, $groupId ){
		$dir = $this->get_dir( $groupId ) . "/members/{$participantId}";
		$listing = $this->fileSystem->get_listing( $dir, '*', TRUE );
		if ( $listing && substr( $listing[0], -21 ) == '-membership-admission' ){
			return TRUE;
		}
		return FALSE;
	}


	public function purge_data( $data ){
		$validKeys = [ 'id' => 0, 'code' => 0, 'name' => 0, 'description' => 0, 'rules' => 0 ];
		return array_intersect_key( $data, $validKeys );
	}
}

// end of file models/groups.class.php

