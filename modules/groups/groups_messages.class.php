<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Groups_Messages {
	
	static function handle_group_definition( $message, $receipt ){
		global $Groups, $FileSystem, $Timestamp;
		$groupData = $message->get('Group-Data');
		if ( empty( $groupData['code'] ) ){
			$receipt->set_rejection( 21, _('Incomplete group data') );
			return $receipt->dump( TRUE );
		}
		if ( preg_match( '/[^a-z0-9._-]/', $groupData['code'] ) ){
			$receipt->set_rejection( 22, _('Invalid characters in group code') . ': [a-z0-9._-]' );
			return $receipt->dump( TRUE );
		}
		$object = [ 'type' => 'server', 'id' => NULL, 'dir' => '/' ];
		$parentGroupId = $message->get('Group-Id');
		if ( $parentGroupId ){
			if ( ! $Groups->get_data( $parentGroupId ) ){
				$receipt->set_rejection( 41, sprintf( _('Parent group %s does not exist'), $parentGroupId ) );
				return $receipt->dump( TRUE );
			}
			$object = [
				'type' => 'group',
				'id' => $parentGroupId,
				'dir' => $Groups->get_dir( $parentGroupId )
			];
		}
		$sender = $message->get('From');
		if ( ! ptcp_is_authorized( $sender, 'group-definition', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to define group here') );
			return $receipt->dump( TRUE );
		}
		$groupId = ( $parentGroupId ? "{$parentGroupId}/" : '' ) . $groupData['code'];
		$dir = $Groups->get_dir( $groupId );
		$success = $FileSystem->make_dir( $dir );
		if ( ! $success ){
			$receipt->set_failure( _('Could not create group directory') . ': ' . $FileSystem->lastError );
			return $receipt->dump( TRUE );
		}

		$data = [
			'id' => $groupId,
			'created_on' => date( 'c', $Timestamp ),
			'created_by' => $sender,
			'modified_on' => null,
			'modified_by' => null,
		];
		$data = array_merge( $data, $Groups->purge_data( $groupData ) );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-group-definition';
		$receipt->set( 'Group-Data', $data );
		$receiptString = $receipt->dump( TRUE );
		$FileSystem->put_contents( "{$dir}/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_group_update_request( $message, $receipt ){
		global $Groups, $FileSystem, $Timestamp;
		$groupId = $message->get('Group-Id');
		$groupData = $Groups->get_data( $groupId );
		if ( ! $groupData ){
			$receipt->set_rejection( 41, sprintf( _('Group %s does not exist'), $groupId ) );
			return $receipt->dump( TRUE );
		}
		$groupDir = $Groups->get_dir( $groupId );
		$object = [ 'type' => 'group', 'id' => $groupId, 'dir' => $groupDir ];
		$sender = $message->get('From');
		if ( ! ptcp_is_authorized( $sender, 'group-update-request', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to update group') );
			return $receipt->dump( TRUE );
		}

		ptcp_update_object( $groupData,
			$Groups->purge_data( $message->get('Group-Data') ) );
		$groupData['modified_on'] = date( 'c', $Timestamp );
		$groupData['modified_by'] = $sender;
		$receipt->set( 'Message-Type', 'group-update-confirmation' );
		$receipt->set( 'Group-Data', $groupData );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-group-update-request';
		$receiptString = $receipt->dump( TRUE );
		$FileSystem->put_contents( "{$groupDir}/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_group_list_request( $message, $receipt ){
		global $Groups;
		$groupId = $message->get('Group-Id');
		if ( $groupId && ( ! $groupData = $Groups->get_data( $groupId ) )
		){
			$receipt->set_rejection( 41, sprintf( _('Group %s does not exist'), $groupId ) );
			return $receipt->dump( TRUE );
		}
		$receipt->set( 'Message-Type', 'group-list' );
		if ( $message->get('Include-Subgroups') ){
			$groupList = $Groups->get_list_recursive( $groupId );
		}
		else {
			$groupList = $Groups->get_list( $groupId );
		}
		if ( $groupList && $message->get('Include-Admin-Info')
			&& $sender = $message->get('From')
		){
			foreach ( $groupList as $key => $group ){
				$groupList[ $key ]['administrable'] = ptcp_is_authorized( $sender, 'group-update-request',
					[ 'type' => 'group', 'id' => $group['id'], 'dir' => $Groups->get_dir( $group['id'] ) ] );
			}
		}
		$receipt->set( 'Groups', $groupList );
		return $receipt->dump( TRUE );
	}


	static function handle_group_details_request( $message, $receipt ){
		global $Admins, $Groups;
		$groupId = $message->get('Group-Id');
		$groupData = $Groups->get_data( $groupId );
		$groupDir = $Groups->get_dir( $groupId );
		if ( ! $groupData ){
			$receipt->set_rejection( 41, sprintf( _('Group %s does not exist'), $groupId ) );
			return $receipt->dump( TRUE );
		}
		if ( $sender = $message->get('From') ){
			$groupData['administrable'] = ptcp_is_authorized( $sender, 'group-update-request',
				[ 'type' => 'group', 'id' => $groupId, 'dir' => $groupDir ] );
		}
		$receipt->set( 'Message-Type', 'group-details' );
		$receipt->set( 'Group-Data', $groupData );
		if ( $message->get('Include-Subgroups') ){
			$groupList = $Groups->get_list_recursive( $groupId );
		}
		else {
			$groupList = $Groups->get_list( $groupId );
		}
		if ( $groupList && $sender && $message->get('Include-Admin-Info') ){
			foreach ( $groupList as $key => $group ){
				$groupList[ $key ]['administrable'] = ptcp_is_authorized( $sender, 'group-update-request',
					[ 'type' => 'group', 'id' => $group['id'], 'dir' => $Groups->get_dir( $group['id'] ) ] );
			}
		}
		$receipt->set( 'Groups', $groupList );
		if ( $message->get('Include-Admins') ){
			$result = $Admins->get_responsibles( $groupDir );
			$receipt->set( 'Admins', $result['list'] );
			$receipt->set( 'Direct-Appointment', $result['direct'] );
		}
		return $receipt->dump( TRUE );
	}


	static function handle_membership_admission( $message, $receipt ){
		global $FileSystem, $Groups, $Timestamp;
		$groupId = $message->get('Group-Id');
		$groupData = $Groups->get_data( $groupId );
		if ( ! $groupData ){
			$receipt->set_rejection( 41, sprintf( _('Group %s does not exist'), $groupId ) );
			return $receipt->dump( TRUE );
		}
		$groupDir = $Groups->get_dir( $groupId );
		$object = [ 'type' => 'group', 'id' => $groupId, 'dir' => $groupDir ];
		if ( ! ptcp_is_authorized( $message->get('From'), 'membership-admission', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to admit memberships') );
			return $receipt->dump( TRUE );
		}
		$participantId = $message->get('Participant-Id');
		if ( $Groups->is_member( $participantId, $groupId ) ){
			$receipt->set_rejection( 42, _('Participant is already a member of that group') );
			return $receipt->dump( TRUE );
		}
		$dir = "{$groupDir}/members/{$participantId}";
		$fileName = date( 'Ymd-His', $Timestamp ) . '-membership-admission';
		$receipt->set( 'Message-Type', 'membership-admission-confirmation' );
		$receiptString = $receipt->dump( TRUE );
		$FileSystem->put_contents( "{$dir}/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_membership_cancellation( $message, $receipt ){
		global $FileSystem, $Groups, $Timestamp;
		$groupId = $message->get('Group-Id');
		$groupData = $Groups->get_data( $groupId );
		if ( ! $groupData ){
			$receipt->set_rejection( 41, sprintf( _('Group %s does not exist'), $groupId ) );
			return $receipt->dump( TRUE );
		}
		$participantId = $message->get('Participant-Id');
		if ( ! $Groups->is_member( $participantId, $groupId ) ){
			$receipt->set_rejection( 42, _('Participant is not a member of that group') );
			return $receipt->dump( TRUE );
		}
		$groupDir = $Groups->get_dir( $groupId );
		$object = [ 'type' => 'group', 'id' => $groupId, 'dir' => $groupDir ];
		$sender = $message->get('From');
		if ( $sender != $participantId
			&& ! ptcp_is_authorized( $sender, 'membership-admission', $object )
		){
			$receipt->set_rejection( 31, _('Sender is not authorized to cancel memberships') );
			return $receipt->dump( TRUE );
		}
		$dir = $Groups->get_dir( $groupId ) . "/members/{$participantId}";
		$fileName = date( 'Ymd-His', $Timestamp ) . '-membership-cancellation';
		$receipt->set( 'Message-Type', 'membership-cancellation-confirmation' );
		$receiptString = $receipt->dump( TRUE );
		$FileSystem->put_contents( "{$dir}/{$fileName}", $receiptString );
		return $receiptString;
	}

}

// end of file groups_messages.class.php

