<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

if ( ! isset( $BaseDir ) ){
	die( "This script must not be called directly\n" );
}

$Access = new ParTCP_Access( $DataDir, $Config['mod_access'] ?? [] );
if ( ! $Access->check_environment() ){
	die( _('Module "access" could not be initialized')
		. " ({$Access->lastError})" );
}

$errors = $Access->delete_old_users();
if ( $errors ){
	echo implode( "\n", $errors );
}

$errors = $Access->process_queue();
if ( $errors ){
	echo implode( "\n", $errors );
}


// end of file access/cron.php

