<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Access_Messages {

	static function handle_access_request( $message, $receipt ){
		global $Access, $FileSystem, $Timestamp;

		list ( $ptcpId ) = explode( '@', $message->get('From') );
		if ( $Access->user_exists( $ptcpId ) ){
			$receipt->set_rejection( 51,
				sprintf( _('Access for %s is already granted'), $ptcpId ) );
			return $receipt->dump( TRUE );
		}

		$result = $Access->enqueue_request( $ptcpId );
		if ( ! empty( $result['error'] ) ){
			if ( $result['error'] == -1 ){
				$receipt->set_rejection( 52,
					sprintf( _('Access request for %s has already been queued'), $ptcpId ) );
				return $receipt->dump( TRUE );
			}
			else {
				$receipt->set_failure( sprintf( _('Access request for %s could not be queued'),
					$ptcpId ) . " ({$result['error']})" );
				return $receipt->dump( TRUE );
			}
		}

		$receipt->set( 'Message-Type', 'access-grant' );
		$receipt->set( 'User', $result['user'] );
		$receipt->set( 'Password', $result['password'], TRUE );
		$receipt->set( 'Notice', $result['notice'] );
		$receiptString = $receipt->dump( TRUE );
		$fileName = date( 'Ymd-His', $Timestamp ) . "-{$ptcpId}";
		$FileSystem->put_contents( "access_requests/{$fileName}", $receiptString );
		return $receiptString;
	}

}

// end of file access_messages.class.php

