<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

require_once __DIR__ . '/access.class.php';
$Access = new ParTCP_Access( $DataDir, $Config['mod_access'] ?? [] );
if ( ! $Access->check_environment() ){
	ptcp_error( _('Module "access" could not be initialized')
		. " ({$Access->lastError})" );
}

require_once __DIR__ . '/access_messages.class.php';
ptcp_register_message_handler( 'ParTCP_Access_Messages' );

$MtdManager->register_dir( __DIR__ . '/mtd' );


// end of file access/init.php

