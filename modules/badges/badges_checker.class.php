<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2023 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Badges_Checker {

	static function is_authorized( $ptcpId, $messageType, $object ){

		$types = [
			'registration' => ['clearing'],
			'event-participant-details-request' => ['clearing','accreditation'],
			'participant-update-request' => ['clearing'],
			'badge-assignment' => ['clearing','accreditation'],
			'badge-details-request' => ['clearing','accreditation','admission'],
			'badge-invalidation' => ['clearing','accreditation'],
			'badge-capture-note' => ['clearing','accreditation','admission'],
			'badge-count-request' => ['clearing','accreditation','admission'],
			'attendee-count-request' => ['clearing','accreditation','admission'],
			'lot-invalidation' => ['clearing','accreditation'],
		];

		if ( ! in_array( $messageType, array_keys( $types ) ) ){
			return NULL;
		}

		$ptcpData = ptcp_get_id_data( $ptcpId );
		$arr = array_intersect( $types[ $messageType ], $ptcpData['flags'] ?? [] );
		return empty( $arr ) ? NULL : TRUE;
	}

}

// end of file lib/badges_checker.class.php

