<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2023 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

require_once __DIR__ . '/badges.class.php';
$Badges = new ParTCP_Badges( $FileSystem, $Partcp );

require_once __DIR__ . '/badges_messages.class.php';
ptcp_register_message_handler( 'ParTCP_Badges_Messages', TRUE );

require_once __DIR__ . '/badges_checker.class.php';
ptcp_register_auth_checker( 'ParTCP_Badges_Checker', TRUE );

$MtdManager->register_dir( __DIR__ . '/mtd' );

// end of file badges/init.php

