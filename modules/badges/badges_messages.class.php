<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2023 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Badges_Messages {

	static function handle_event_participant_details_request( $message, $receipt ){
		global $Badges, $Events, $ServerData, $Timestamp;
		list ( $ptcpId, $server ) = explode( '@', $message->get('Participant-Id') )
			+ [ '', '' ];
		if ( $server != '' && $server != $ServerData['name'] ){
			$receipt->set_rejection( 41, _('Participant not hosted on this machine') );
			return $receipt->dump( TRUE );
		}
		$ptcpData = ptcp_get_id_data( $message->get('Participant-Id') );
		if ( ! $ptcpData ){
			$receipt->set_rejection( 42, sprintf( _('Unknown participant %s'), $ptcpId ) );
			return $receipt->dump( TRUE );
		}
		$eventId = $message->get('Event-Id');
		$eventData = $Events->get_data( $eventId );
		if ( ! $eventData ){
			$receipt->set_rejection( 22,
				sprintf( _('Event %s does not exist'),	$eventId ) );
			return $receipt->dump( TRUE );
		}
		$eventDir = $Events->get_dir( $eventId );
		$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ];
		if ( ! ptcp_is_authorized( $message->get('From'), 'event-participant-details-request', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to request event-related participant details') );
			return $receipt->dump( TRUE );
		}
		$Badges->set_base_dir( $Events->get_dir( $eventId ) );
		$badgeCode = $Badges->get_holder_badge_code( $ptcpId );
		$targetCategory = $Badges->attr2category( $ptcpData, $eventData );
		if ( ! $badgeCode ){
			$ptcpData['badge'] = [
				'code' => 0,
				'category' => $targetCategory['id'],
				'category_name' => $targetCategory['name'],
				'color' => $targetCategory['color'],
			];
		}
		else {
			$ptcpData['badge'] = $Badges->get_badge_data( $badgeCode );
			if ( $ptcpData['badge']['category'] != $targetCategory['id'] ){
				$ptcpData['target_badge'] = [
					'category' => $targetCategory['id'],
					'category_name' => $targetCategory['name'],
					'color' => $targetCategory['color'],
				];
			}
		}
		$accreditationTime = $Badges->get_holder_accreditation_time( $ptcpId );
		if ( ! $accreditationTime ){
			$accreditationTime = $Timestamp;
		}
		$ptcpData['accredited_on'] = date( 'c', $accreditationTime );
		$receipt->set( 'Message-Type', 'event-participant-details' );
		$receipt->set( 'Participant-Data', $ptcpData, TRUE );
		return $receipt->dump( TRUE );
	}


	static function handle_badge_assignment( $message, $receipt ){
		global $Badges, $Events, $FileSystem, $Timestamp;

		$eventId = $message->get('Event-Id');
		$ptcpId = $message->get('Participant-Id');
		$badgeCode = $message->get('Badge-Code');

		$eventData = $Events->get_data( $eventId );
		if ( ! $eventData ){
			$receipt->set_rejection( 22,
				sprintf( _('Event %s does not exist'),	$eventId ) );
			return $receipt->dump( TRUE );
		}
		$eventDir = $Events->get_dir( $eventId );
		$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ];
		if ( ! ptcp_is_authorized( $message->get('From'), 'badge-assignment', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to assign badges') );
			return $receipt->dump( TRUE );
		}

		$ptcpData = ptcp_get_id_data( $ptcpId );
		if ( ! $ptcpData ){
			$receipt->set_rejection( 23,
				sprintf( _('Participant %s does not exist'), $ptcpId ) );
			return $receipt->dump( TRUE );
		}

		$Badges->set_base_dir( $eventDir );
		$badgeData = $Badges->get_badge_data( $badgeCode );
		if ( $badgeData == -2 ){
			$receipt->set_rejection( 44, _('Badge has been invalidated') );
			return $receipt->dump( TRUE );
		}
		if ( isset( $badgeData['participant_id'] )
			&& $badgeData['participant_id'] != $ptcpId
		){
			$receipt->set_rejection( 42,
				_('Badge has already been assigned to another participant') );
			return $receipt->dump( TRUE );
		}

		// Check if participant has got a badge assigned before
		// and invalidate it, if neccessary

		$oldBadgeCode = $Badges->get_holder_badge_code( $ptcpId );
		if ( $oldBadgeCode ){
			$replacement = TRUE;
			if ( $oldBadgeCode == $badgeCode ){
				$nothingToDo = TRUE;
				goto PROCEED;
			}
			if ( empty( $message->get( 'Invalidate-Existing' ) ) ){
				$receipt->set_rejection( 43,
					_('Participant has already been assigned a badge') );
				return $receipt->dump( TRUE );
			}
			$oldBadgeData = $Badges->get_badge_data( $oldBadgeCode, TRUE );
			if ( in_array( $oldBadgeData, [ -1, -2 ] ) ){
				goto PROCEED; // Badge does not exist or is invalidated already
			}
			if ( ! empty( $oldBadgeData['current_session'] )
				&& in_array( $oldBadgeData['current_session'], $eventData['active_sessions'] )
			){
				// Badge has been captured for an active session – reset the capture
				$result = $Badges->capture_badge( $oldBadgeCode,
					$oldBadgeData['current_session'], 'out', $oldBadgeData['category'] );
				$receipt->set( 'Message-Type', 'surrogate-badge-capture-confirmation' );
				$receiptString = $receipt->dump( TRUE );
				foreach ( $result as $file ){
					$file = str_replace( '%TS%', date( 'Ymd-His', $Timestamp ), $file );
					$FileSystem->put_contents( $file, $receiptString );
				}
			}
			$result = $Badges->invalidate_badge( $oldBadgeCode, FALSE );
			$receipt->set( 'Message-Type', 'surrogate-badge-invalidation-confirmation' );
			$receiptString = $receipt->dump( TRUE );
			foreach ( $result as $file ){
				$file = str_replace( '%TS%', date( 'Ymd-His', $Timestamp ), $file );
				$FileSystem->put_contents( $file, $receiptString );
			}

			// If there is a mutex event, unblock participant over there
			if ( ! empty( $eventData['mutex_event'] ) ){
				$result = $Badges->unblock_participant( $ptcpId, $eventData['mutex_event'] );
				if ( $result < 0 ){
					$receipt->set( 'Warning',
						_('Participant could not be unblocked for mutex event')
						. " ({$result})" );
				}
			}
		}

		PROCEED:

		// If there is a mutex event, block participant over there

		if ( ! empty( $eventData['mutex_event'] ) ){
			$result = $Badges->block_participant( $ptcpId, $eventData['mutex_event'] );
			if ( $result < 0 ){
				$receipt->set_rejection( 45,
					_('Participant could not be blocked for mutex event')
					. " ({$result})" );
			}
		}

		$category = $Badges->attr2category( $ptcpData, $eventData );
		$result = empty( $nothingToDo ) ? $Badges->assign_badge( $badgeCode, $ptcpId ) : [];
		$receipt->set( 'Message-Type', 'badge-details' );
		$receipt->set( 'Badge-Data', [
			'code' => $badgeCode,
			'issued_on' => date( 'c', $Timestamp ),
			'event_id' => $eventId,
			'participant_id' => $ptcpId,
			'category' => $category['id'],
			'category_name' => $category['name'],
			'color' => $category['color'],
		]);
		$receiptString = $receipt->dump( TRUE );
		foreach ( $result as $file ){
			$file = str_replace( '%TS%', date( 'Ymd-His', $Timestamp ), $file );
			$FileSystem->put_contents( $file, $receiptString );
		}
		return $receiptString;
	}


	static function handle_badge_details_request( $message, $receipt ){
		global $Badges, $Events;
		$eventId = $message->get('Event-Id');
		$eventData = $Events->get_data( $eventId );
		if ( ! $eventData ){
			$receipt->set_rejection( 22, _('Event does not exist') );
			return $receipt->dump( TRUE );
		}
		$eventDir = $Events->get_dir( $eventId );
		$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ];
		if ( ! ptcp_is_authorized( $message->get('From'), 'badge-details-request', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to request badge details') );
			return $receipt->dump( TRUE );
		}
		$Badges->set_base_dir( $Events->get_dir( $eventId ) );
		$data = $Badges->get_badge_data( $message->get('Badge-Code'), TRUE );
		if ( $data == -1 ){
			$receipt->set_rejection( 41, _('Badge does not exist') );
			return $receipt->dump( TRUE );
		}
		if ( $data == -2 ){
			$receipt->set_rejection( 42, _('Badge has been invalidated') );
			return $receipt->dump( TRUE );
		}
		$receipt->set( 'Message-Type', 'badge-details' );
		$receipt->set( 'Badge-Data', $data );
		return $receipt->dump( TRUE );
	}


	static function handle_badge_invalidation( $message, $receipt ){
		global $Badges, $Events, $FileSystem, $Timestamp;
		$eventId = $message->get('Event-Id');
		$badgeCode = $message->get('Badge-Code');
		$eventData = $Events->get_data( $eventId );
		if ( ! $eventData ){
			$receipt->set_rejection( 22, _('Event does not exist') );
			return $receipt->dump( TRUE );
		}
		$eventDir = $Events->get_dir( $eventId );
		$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ];
		if ( ! ptcp_is_authorized( $message->get('From'), 'badge-invalidation', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to invalidate badges') );
			return $receipt->dump( TRUE );
		}
		$Badges->set_base_dir( $eventDir );
		$badgeData = $Badges->get_badge_data( $badgeCode, TRUE );
		if ( $badgeData == -1 ){
			$receipt->set_rejection( 41, _('Badge does not exist') );
			return $receipt->dump( TRUE );
		}
		if ( $badgeData == -2 ){
			$receipt->set_rejection( 42, _('Badge has been invalidated') );
			return $receipt->dump( TRUE );
		}
		if ( ! empty( $badgeData['current_session'] )
			&& in_array( $badgeData['current_session'], $eventData['active_sessions'] )
		){
			$receipt->set_rejection( 43, _('Badge has been captured for an active session') );
			return $receipt->dump( TRUE );
		}

		// If there is a mutex event, unblock participant over there

		if ( ! empty( $eventData['mutex_event'] ) ){
			$result = $Badges->unblock_participant( $badgeData['participant_id'],
				$eventData['mutex_event'] );
			if ( $result < 0 ){
				$receipt->set( 'Warning',
					_('Participant could not be unblocked for mutex event')
					. " ({$result})" );
			}
		}

		$result = $Badges->invalidate_badge( $badgeCode );
		$receipt->set( 'Message-Type', 'badge-invalidation-confirmation' );
		$receiptString = $receipt->dump( TRUE );
		foreach ( $result as $file ){
			$file = str_replace( '%TS%', date( 'Ymd-His', $Timestamp ), $file );
			$FileSystem->put_contents( $file, $receiptString );
		}
		return $receiptString;
	}


	static function handle_badge_capture_note( $message, $receipt ){
		global $Badges, $Events, $FileSystem, $Timestamp;
		$eventId = $message->get('Event-Id');
		$eventData = $Events->get_data( $eventId );
		if ( ! $eventData ){
			$receipt->set_rejection( 22, _('Event does not exist') );
			return $receipt->dump( TRUE );
		}
		$eventDir = $Events->get_dir( $eventId );
		$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ];
		if ( ! ptcp_is_authorized( $message->get('From'), 'badge-capture-note', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to capture badges') );
			return $receipt->dump( TRUE );
		}
		if ( empty( $eventData['active_sessions'] ) ){
			$receipt->set_rejection( 46, _('Event has no active sessions') );
			return $receipt->dump( TRUE );
		}
		$sessionId = $message->get('Session-Id');
		if ( $sessionId && ! in_array( $sessionId, $eventData['active_sessions'] ) ){
			$receipt->set_rejection( 24, _('Invalid session identifier') );
			return $receipt->dump( TRUE );
		}
		if ( ! $sessionId ){
			$sessionId = $eventData['active_sessions'][0];
		}
		$badgeCode = $message->get('Badge-Code');
		$ignoreConflict = $message->get('Ignore-Conflict');
		$direction = $message->get('Direction');
		if ( ! in_array( $direction, [ 'in', 'out' ] ) ){
			$receipt->set_rejection( 23, _('Invalid value for Direction element (must be \'in\' or \'out\')') );
			return $receipt->dump( TRUE );
		}
		$Badges->set_base_dir( $eventDir );
		$badgeData = $Badges->get_badge_data( $badgeCode, TRUE );
		if ( $badgeData == -1 ){
			$receipt->set_rejection( 41, _('Badge does not exist') );
			return $receipt->dump( TRUE );
		}
		if ( $badgeData == -2 ){
			$receipt->set_rejection( 42, _('Badge has been invalidated') );
			return $receipt->dump( TRUE );
		}
		if ( ! empty( $eventData['badge_expiration'] )
			&& substr( $badgeData['issued_on'], 0, 10 ) != date('Y-m-d')
		){
			$receipt->set_rejection( 49, _('Badge has expired') );
			return $receipt->dump( TRUE );
		}
		if ( $direction == 'in' && ! empty( $badgeData['current_session'] ) ){
			if ( $badgeData['current_session'] == $sessionId ){
				$conflict = 1; // Badge has already been captured for this session
			}
			elseif ( in_array( $badgeData['current_session'],
				$eventData['active_sessions'] )
			){
				$conflict = 2; // Badge has been captured for another session
			}
		}
		elseif ( $direction == 'out' && empty( $badgeData['current_session'] ) ){
			$conflict = 3; // Badge has not been captured for this session
		}

		$receipt->set( 'Message-Type', 'badge-capture-confirmation' );
		$receipt->set( 'Badge-Color', $badgeData['color'] );
		if ( ! empty( $conflict ) && ! $ignoreConflict ){
			$receipt->set( 'Current-Session', $badgeData['current_session'] );
			$receipt->set( 'Conflict', $conflict );
			$direction = 'conflict';
		}
		else {
			$receipt->set( 'Current-Session', $direction == 'in' ? $sessionId : '' );
			$receipt->set( 'Conflict', 0 );
		}
		$receiptString = $receipt->dump( TRUE );
		$result = $Badges->capture_badge( $badgeCode, $sessionId,
			$direction, $badgeData['category'] );
		foreach ( $result as $file ){
			$file = str_replace( '%TS%', date( 'Ymd-His', $Timestamp ), $file );
			$FileSystem->put_contents( $file, $receiptString );
		}
		return $receiptString;
	}


	static function handle_badge_count_request( $message, $receipt ){
		global $Badges, $Events;
		$eventId = $message->get('Event-Id');
		$eventData = $Events->get_data( $eventId );
		if ( ! $eventData ){
			$receipt->set_rejection( 22, _('Event does not exist') );
			return $receipt->dump( TRUE );
		}
		$eventDir = $Events->get_dir( $eventId );
		$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ];
		if ( ! ptcp_is_authorized( $message->get('From'), 'badge-count-request', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to count badges') );
			return $receipt->dump( TRUE );
		}
		$receipt->set( 'Message-Type', 'badge-count-results' );
		$receipt->set( 'Event-Id', $eventId );
		$Badges->set_base_dir( $eventDir );
		$receipt->set( 'Badge-Count', $Badges->count_holders(
			$eventData['badge_categories'], ! empty( $eventData['badge_expiration'] ) ) );
		return $receipt->dump( TRUE );
	}


	static function handle_badge_report_request( $message, $receipt ){
		global $Badges, $Events;
		$eventId = $message->get('Event-Id');
		$attribute = $message->get('Attribute');
		$pattern = $message->get('Pattern');
		$categories = $message->get('Categories');
		$eventData = $Events->get_data( $eventId );
		if ( ! $eventData ){
			$receipt->set_rejection( 22, _('Event does not exist') );
			return $receipt->dump( TRUE );
		}
		$eventDir = $Events->get_dir( $eventId );
		$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ];
		if ( ! ptcp_is_authorized( $message->get('From'), 'badge-report-request', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to request reports') );
			return $receipt->dump( TRUE );
		}
		$receipt->set( 'Message-Type', 'badge-zip-report' );
		$receipt->set( 'Event-Id', $eventId );
		$receipt->set( 'Attribute', $attribute );
		if ( $pattern ){
			$receipt->set( 'Pattern', $pattern );
		}
		if ( $categories ){
			$receipt->set( 'Categories', $categories );
		}
		$Badges->set_base_dir( $eventDir );
		$receipt->set( 'Report', $Badges->count_holders_by_attribute( $attribute, $pattern,
			$categories, ! empty( $eventData['badge_expiration'] ) ) );
		return $receipt->dump( TRUE );
	}


	static function handle_session_reset_request( $message, $receipt ){
		global $Badges, $Events, $FileSystem, $Timestamp;
		$eventId = $message->get('Event-Id');
		$eventData = $Events->get_data( $eventId );
		if ( ! $eventData ){
			$receipt->set_rejection( 22, _('Event does not exist') );
			return $receipt->dump( TRUE );
		}
		$eventDir = $Events->get_dir( $eventId );
		$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ];
		if ( ! ptcp_is_authorized( $message->get('From'), 'session-reset-request', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to reset sessions') );
			return $receipt->dump( TRUE );
		}
		$sessionId = $message->get('Session-Id');
		if ( ! $sessionId ){
			$sessionId = $eventData['active_sessions'][0] ?? NULL;
		}
		if ( ! $sessionId  ){
			$receipt->set_rejection( 23, _('No session specified') );
			return $receipt->dump( TRUE );
		}
		$Badges->set_base_dir( $eventDir );
		$result = $Badges->reset_session( $sessionId );
		$receipt->set( 'Message-Type', 'surrogate-badge-capture-confirmation' );
		$receipt->set( 'Event-Id', $eventId );
		$receipt->set( 'Session-Id', $sessionId );
		$receipt->set( 'Current-Session', '' );
		$receiptString = $receipt->dump( TRUE );
		foreach ( $result as $file ){
			$file = str_replace( '%TS%', date( 'Ymd-His', $Timestamp ), $file );
			$FileSystem->put_contents( $file, $receiptString );
		}
		$receipt->set( 'Message-Type', 'session-reset-confirmation' );
		$receipt->set( 'Current-Session', NULL );
		$receipt->set( 'Reset-Badges', count( $result ) );
		$receiptString = $receipt->dump( TRUE );
		$file = "{$eventDir}/badges/sessions/{$sessionId}/"
			. date( 'Ymd-His', $Timestamp ) . '-session-reset';
		$FileSystem->put_contents( $file, $receiptString );
		return $receiptString;
	}


	static function handle_attendee_count_request( $message, $receipt ){
		global $Badges, $Events;
		$eventId = $message->get('Event-Id');
		$deadline = $message->get('Deadline');
		$eventData = $Events->get_data( $eventId );
		if ( ! $eventData ){
			$receipt->set_rejection( 22, _('Event does not exist') );
			return $receipt->dump( TRUE );
		}
		$eventDir = $Events->get_dir( $eventId );
		$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ];
		if ( ! ptcp_is_authorized( $message->get('From'), 'attendee-count-request', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to count attendees') );
			return $receipt->dump( TRUE );
		}
		if ( empty( $eventData['active_sessions'] ) ){
			$receipt->set_rejection( 41, _('Event has no active sessions') );
			return $receipt->dump( TRUE );
		}
		$sessionId = $message->get('Session-Id');
		if ( $sessionId && ! in_array( $sessionId, $eventData['active_sessions'] ) ){
			$receipt->set_rejection( 23, _('Invalid session identifier') );
			return $receipt->dump( TRUE );
		}
		if ( ! $sessionId ){
			$sessionId = $eventData['active_sessions'][0];
		}
		$receipt->set( 'Message-Type', 'attendee-count-results' );
		$receipt->set( 'Event-Id', $eventId );
		$receipt->set( 'Session-Id', $sessionId );
		if ( $deadline ){
			$receipt->set( 'Deadline', date( 'c', $deadline ) );
		}
		$Badges->set_base_dir( $eventDir );
		$receipt->set( 'Attendee-Count', $Badges->count_attendees( $sessionId,
			$eventData['badge_categories'], $deadline ) );
		return $receipt->dump( TRUE );
	}

}

// end of file badges_messages.class.php

