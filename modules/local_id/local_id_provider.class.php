<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Local_Id_Provider {
	
	public $localIdModel;
	
	
	public function __construct( $localIdModel ){
		$this->localIdModel = $localIdModel;
	}

	function get_info( $id, $message = NULL ){
		global $Config, $Events, $ServerData;
		list ( $id, $server ) = explode( '@', $id ) + ['',''];
		if ( $server && $server != $ServerData['name'] ){
			return FALSE;
		}
		// if an event id is provided, check event participants first
		if ( is_object( $Events ) ){
			if ( strpos( $id, '+' ) ){
				list ( $eventId, $id ) = explode( '+', $id );
			}
			if ( isset( $eventId ) || $message && $eventId = $message->get('Event-Id') ){
				$this->localIdModel->set_base_dir( $Events->get_dir( $eventId ) );
				$data = $this->localIdModel->get_data( $id );
				if ( ! empty( $data['id'] ) ){
					return $data;
				}
				$this->localIdModel->set_base_dir();
			}
		}
		$data = $this->localIdModel->get_data( $id );
		if ( empty( $data['id'] ) ){
			return FALSE;
		}
		return $data;
	}

}

// end of file local_id_provider.class.php

