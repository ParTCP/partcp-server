<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

require_once __DIR__ . '/local_id.class.php';
$LocalId = new ParTCP_Local_Id( $FileSystem, $DataDir );

require_once __DIR__ . '/local_id_messages.class.php';
ptcp_register_message_handler( 'ParTCP_Local_Id_Messages', TRUE );
$MtdManager->register_dir( __DIR__ . '/mtd' );

require_once __DIR__ . '/local_id_provider.class.php';
ptcp_register_id_provider( new ParTCP_Local_Id_Provider(
	new ParTCP_Local_Id( $FileSystem, $DataDir ) ) );

if ( $Config['estimated_turnout'] ?? $Config['Estimated-Turnout'] ?? NULL ){
	$LocalId->set_base_dir( '', $Config['estimated_turnout'] ?? $Config['Estimated-Turnout'] );
}

if ( isset( $Server ) ){
	$Server->register_object( [ $LocalId, 'get_metadata' ], 'Local-Participants' );
}

// end of file local_id/init.php

