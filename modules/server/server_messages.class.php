<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Server_Messages {

	static function handle_server_details_request( $message, $receipt ){
		global $Events, $Groups, $Server, $ServerData;
		if ( $sender = $message->get('From') ){
			$administrable = ptcp_is_authorized( $sender, 'group-definition',
				[ 'type' => 'server', 'id' => '', 'dir' => '/' ] );
		}
		$data = array_merge( $ServerData, [ 'administrable' => $administrable ?? FALSE ] );
		$receipt->set( 'Message-Type', 'server-details' );
		$receipt->set( 'Server-Data', $data );
		$receipt->set_public_key();
		$receipt->multiset( $Server->get_objects() );
		if ( is_object( $Groups ) && method_exists( $Groups, 'get_list' ) ){
			if ( $message->get('Include-Subgroups') ){
				$groupList = $Groups->get_list_recursive();
			}
			else {
				$groupList = $Groups->get_list();
			}
			if ( $groupList && $sender && $message->get('Include-Admin-Info') ){
				foreach ( $groupList as $key => $group ){
					$groupList[ $key ]['administrable'] = ptcp_is_authorized( $sender, 'group-update-request',
						[ 'type' => 'group', 'id' => $group['id'], 'dir' => $Groups->get_dir( $group['id'] ) ] );
				}
			}
			$receipt->set( 'Groups', $groupList );
		}
		if ( $message->get('Include-Events') && is_object( $Events )
			&& method_exists( $Events, 'get_list' )
		){
			$status = $message->get('Event-Statuses') ?: $message->get('Event-Status')
				?: [ 'opened', 'closed' ];
			$eventList = $Events->get_list( NULL, $status );
			if ( isset( $groupList ) ){
				foreach ( $groupList as $group ){
					$eventList = array_merge( $eventList,
						$Events->get_list( $group['id'], $status ) );
				}
			}
			$receipt->set( 'Events', $eventList );
		}
		return $receipt->dump( TRUE );
	}


	static function handle_server_update_request( $message, $receipt ){
		global $Config, $FileSystem, $Server, $Timestamp;
		$sender = $message->get('From');
		$object = [ 'type' => 'server', 'id' => '', 'dir' => '/' ];
		if ( ! ptcp_is_authorized( $sender, 'server-update-request',
			$object )
		){
			$receipt->set_rejection( 31, _('Sender is not authorized to update server') );
			return $receipt->dump( TRUE );
		}
		$data = $Server->get_data() ?: $Config['server_data'] ?? $Config['Server-Data'] ?? NULL;
		if ( ! $data ){
			$receipt->set_failure( _('Could not get server data') );
			return $receipt->dump( TRUE );
		}

		ptcp_update_object( $data, $Server->purge_data( $message->get('Server-Data') ) );
		$data['modified_on'] = date( 'c', $Timestamp );
		$data['modified_by'] = $sender;
		$receipt->set( 'Message-Type', 'server-update-confirmation' );
		$receipt->set( 'Server-Data', $data );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-server-update-request';
		$receiptString = $receipt->dump( TRUE );
		$FileSystem->put_contents( "_def/{$fileName}", $receiptString );
		return $receiptString;
	}


}

// end of file server_messages.class.php

