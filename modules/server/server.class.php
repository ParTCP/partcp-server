<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Server {

	public $fileSystem;
	public $options;
	public $objects;


	public function __construct( $fileSystem, $options = [] ){
		$this->fileSystem = $fileSystem;
		$this->options = $options;
		$this->objects = [];
	}


	public function register_object( $callback, $name ){
		$this->objects[] = [ $callback, $name ];
	}


	public function get_dir( $absolute = FALSE ){
		return '';
	}


	public function get_data(){
		if ( ! $this->fileSystem->exists('_def') ){
			return FALSE;
		}
		$data = $this->fileSystem->get_recent_contents( '_def', "[0-9]*" );
		if ( empty( $data ) ){
			return FALSE;
		}
		$receipt = yaml_parse( $data );
		if ( empty( $receipt['Server-Data'] ) ){
			return FALSE;
		}
		$data = $receipt['Server-Data'];
		if ( empty( $data['name'] ) ){
			$data['name'] = $_SERVER['SERVER_NAME'];
		}
		return $data;
	}


	public function get_objects(){
		$result = [];
		foreach ( $this->objects as $object ){
			$data = call_user_func( $object[0] );
			if ( $data ){
				$result[ $object[1] ] = $data;
			}
		}
		return $result;
	}


	public function purge_data( $data ){
		$validKeys = [ 'name' => 0, 'community' => 0, 'modules' => 0, 'locale' => 0,
			'consent_statement' => 0, 'privacy_notice' => 0, 'key_manager' => 0,
			'short_code' => 0, 'description' => 0 ];
		return array_intersect_key( $data, $validKeys );
	}
}

// end of file server.class.php

