<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

require_once __DIR__ . '/server.class.php';
$Server = new ParTCP_Server( $FileSystem );
$loadedModules = $ServerData['modules'];
$ServerData = $Server->get_data() ?: $ServerData;

require_once __DIR__ . '/server_messages.class.php';
ptcp_register_message_handler( 'ParTCP_Server_Messages' );
$MtdManager->register_dir( __DIR__ . '/mtd' );

if ( ! empty( $ServerData['modules'] ) ){
	foreach ( $ServerData['modules'] as $module ){
		if ( in_array( $module, $loadedModules ) ){
			continue;
		}
		$path = "{$BaseDir}/modules/{$module}/init.php";
		if ( file_exists( $path ) ){
			include $path;
		}
	}
}

$ServerData['modules'] = array_values( array_unique(
	array_merge( $loadedModules, $ServerData['modules'] )
));


// end of file server/init.php

