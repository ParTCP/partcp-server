<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Issue_Services_Messages {

	static function handle_issue_service_definition( $message, $receipt ){
		global $Events, $FileSystem, $IssueServices, $ServerData, $Timestamp;

		$sender = $message->get('From');
		$object = [ 'type' => 'issue_service', 'id' => 0, 'dir' => '/' ];
		if ( ! ptcp_is_authorized( $sender, 'issue-service-definition', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to define issue services') );
			return $receipt->dump( TRUE );
		}
		$data = $message->get('Issue-Service-Data');
		if ( empty( $data['voting_server'] ) || empty( $data['event_id'] ) ){
			$receipt->set_rejection( 21, _('Missing attributes') );
			return $receipt->dump( TRUE );
		}
		$serviceId = $IssueServices->get_id( $data['voting_server'], $data['event_id'] );
		$serviceDir = $IssueServices->get_dir( $serviceId );
		if ( $FileSystem->exists( $serviceDir ) ){
			$receipt->set_rejection( 41, _('Issue service is already defined for this event') );
			return $receipt->dump( TRUE );
		}
		$serviceData = [
			'id' => $serviceId,
			'created_on' => date( 'c', $Timestamp ),
			'created_by' => $sender,
			'modified_on' => null,
			'modified_by' => null,
			'voting_server' => $data['voting_server'],
			'event_id' => $data['event_id'],
			'status' => 'active',
		];
		if ( $data['voting_server'] == 'localhost' ){
			$eventData = $Events->get_data( $data['event_id'] );
		}
		else {
			$eventData = $IssueServices->fetch_event_data( $serviceData );
		}
		if ( ! is_array( $eventData ) ){
			$receipt->set_rejection( 42, _('Could not fetch event data from voting server') );
			return $receipt->dump( TRUE );
		}
		if ( empty( $eventData['issue_service']['host'] )
			|| ( $eventData['issue_service']['host'] == 'localhost'
			&& $data['voting_server'] != 'localhost' )
			|| ( $eventData['issue_service']['host'] != 'localhost'
			&& $eventData['issue_service']['host'] != $ServerData['name'] )
		){
			$receipt->set_rejection( 43, _('Event is not linked to this issue server') );
			return $receipt->dump( TRUE );
		}
		if ( empty( $eventData['issue_service']['id_provider'] )
			|| empty( $eventData['issue_service']['terms'] )
		){
			$receipt->set_rejection( 44, _('ID provider or terms are undefined') );
			return $receipt->dump( TRUE );
		}
		if ( ! $FileSystem->make_dir( $serviceDir ) ){
			$receipt->set_failure( _('Could not create directory for issue service') );
			return $receipt->dump( TRUE );
		}
		$serviceData['id_provider'] = $eventData['issue_service']['id_provider'] ?? NULL;
		$serviceData['terms'] = $eventData['issue_service']['terms'] ?? NULL;
		$fileName = date( 'Ymd-His', $Timestamp ) . '-issue-service-definition';
		$receipt->set( 'Message-Type', 'issue-service-details' );
		$receipt->set( 'Issue-Service-Data', $serviceData );
		$receiptString = $receipt->dump( TRUE );
		$FileSystem->put_contents( "{$serviceDir}/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_issue_service_update_request( $message, $receipt ){
		global $Events, $FileSystem, $IssueServices, $ServerData, $Timestamp;

		$serviceId = $message->get('Issue-Service-Id');
		$serviceData = $IssueServices->get_data( $serviceId );
		if ( ! $serviceData ){
			$receipt->set_rejection( 41, sprintf( _('Issue service %s does not exist'), $serviceId ) );
			return $receipt->dump( TRUE );
		}
		$serviceDir = $IssueServices->get_dir( $serviceId );
		$object = [ 'type' => 'issue_service', 'id' => $serviceId, 'dir' => $serviceDir ];
		$sender = $message->get('From');
		if ( ! ptcp_is_authorized( $sender, 'issue-service-update-request', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to update issue service') );
			return $receipt->dump( TRUE );
		}

		if ( $serviceData['voting_server'] == 'localhost' ){
			$eventData = $Events->get_data( $serviceData['event_id'] );
		}
		else {
			$eventData = $IssueServices->fetch_event_data( $serviceData);
		}
		if ( ! is_array( $eventData ) ){
			$receipt->set_rejection( 42, _('Could not fetch event data from voting server') );
			return $receipt->dump( TRUE );
		}
		$new['status'] = 'active';
		if ( empty( $eventData['issue_service']['host'] )
			|| ( $eventData['issue_service']['host'] == 'localhost'
			&& $serviceData['voting_server'] != 'localhost' )
			|| ( $eventData['issue_service']['host'] != 'localhost'
			&& $eventData['issue_service']['host'] != $ServerData['name'] )
			|| empty( $eventData['issue_service']['id_provider'] )
			|| empty( $eventData['issue_service']['terms'] )
		){
			$new['status'] = 'inactive';
		}

		$new['id_provider'] = $eventData['issue_service']['id_provider'] ?? NULL;
		$new['terms'] = $eventData['issue_service']['terms'] ?? NULL;
		if ( $new['status'] == $serviceData['status']
			&& $new['id_provider'] == $serviceData['id_provider']
			&& $new['terms'] == $serviceData['terms']
		){
			$receipt->set_rejection( 43, _('Issue service is up-to-date') );
			return $receipt->dump( TRUE );
		}
		$serviceData['modified_on'] = date( 'c', $Timestamp );
		$serviceData['modified_by'] = $sender;
		$serviceData['status'] = $new['status'];
		$serviceData['id_provider'] = $new['id_provider'];
		$serviceData['terms'] = $new['terms'];
		$receipt->set( 'Message-Type', 'issue-service-details' );
		$receipt->set( 'Issue-Service-Data', $serviceData );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-issue-service-update-request';
		$receiptString = $receipt->dump( TRUE );
		$FileSystem->put_contents( "{$serviceDir}/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_issue_service_list_request( $message, $receipt ){
		global $IssueServices;
		$list = $IssueServices->get_list();
		$receipt->set( 'Issue-Services', $list );
		return $receipt->dump( TRUE );
	}


	static function handle_issue_service_details_request( $message, $receipt ){
		global $IssueServices;
		$serviceId = $message->get('Issue-Service-Id');
		$serviceData = $IssueServices->get_data( $serviceId );
		if ( ! $serviceData ){
			$receipt->set_rejection( 41, _('Issue service does not exist') );
			return $receipt->dump( TRUE );
		}
		$receipt->set( 'Issue-Service-Data', $serviceData );
		return $receipt->dump( TRUE );
	}


	static function handle_issue_service_lot_code_request( $message, $receipt ){
		global $FileSystem, $IssueServices, $Timestamp;

		$eventId = $message->get('Event-Id');
		$server = $message->get('Event-Server') ?: 'localhost';
		$serviceId = $IssueServices->get_id( $server, $eventId );
		$serviceData = $IssueServices->get_data( $serviceId );
		$serviceDir = $IssueServices->get_dir( $serviceId );
		if ( ! $serviceData ){
			$receipt->set_rejection( 41, _('No issue service found for this server/event') );
			return $receipt->dump( TRUE );
		}
		if ( $serviceData['status'] != 'active' ){
			$receipt->set_rejection( 42, _('Issue service is inactive') );
			return $receipt->dump( TRUE );
		}
		$ptcpId = $message->get('From');
		$server = substr( strrchr( $ptcpId, '@' ), 1 );
		if ( $server != $serviceData['id_provider'] ){
			$receipt->set_rejection( 43,
				sprintf( _('Only IDs from %s are entitled to request lot code for this event'),
				$serviceData['id_provider'] ) );
			return $receipt->dump( TRUE );
		}
		$dir = "{$serviceDir}/participants/{$ptcpId}";
		$filename = $FileSystem->get_recent_filename( $dir );
		if ( $filename && substr( $filename, -12 ) == '-block-order' ){
			$receipt->set_rejection( 44, _('Participant has been blocked') );
			return $receipt->dump( TRUE );
		}
		if ( $filename && substr( $filename, -9 ) == '-lot-code' ){
			$receipt->set_rejection( 45, _('Participant has already received a lot code') );
			return $receipt->dump( TRUE );
		}
		if ( ! $IssueServices->check_if_terms_are_matched( $ptcpId, $serviceData['terms'] ) ){
			$receipt->set_rejection( 46, _('Sender is not entitled to participate in the event') );
			return $receipt->dump( TRUE );
		}
		$lotCode = $IssueServices->get_lot_code( $serviceId );
		if ( ! $lotCode ){
			$receipt->set_failure( _('Lot code could not be fetched') );
			return $receipt->dump( TRUE );
		}
		if ( $IssueServices->lastResponse
			&& $IssueServices->lastResponse->get('Message-Type') == 'participant-details'
		){
			$path = $dir . '/' . date( 'Ymd-His', $Timestamp - 1 ) . '-participant-details';
			$FileSystem->put_contents( $path, $IssueServices->lastResponse->rawMessage );
		}
		$receipt->set( 'Lot-Code', '********' );
		$path = $dir . '/' . date( 'Ymd-His', $Timestamp ) . '-lot-code';
		$FileSystem->put_contents( $path, $receipt->dump( TRUE ) );
		$receipt->set( 'Lot-Code', $lotCode, TRUE );
		return $receipt->dump( TRUE );
	}


	static function handle_issue_service_test_request( $message, $receipt ){
		global $IssueServices;

		$eventId = $message->get('Event-Id');
		$server = $message->get('Event-Server') ?: 'localhost';
		$serviceId = $IssueServices->get_id( $server, $eventId );
		$serviceDir = $IssueServices->get_dir( $serviceId );
		$object = [ 'type' => 'issue_service', 'id' => $serviceId, 'dir' => $serviceDir ];
		$sender = $message->get('From');
		if ( ! ptcp_is_authorized( $sender, 'issue-service-test-request', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to test issue service') );
			return $receipt->dump( TRUE );
		}
		$serviceData = $IssueServices->get_data( $serviceId );
		if ( ! $serviceData ){
			$receipt->set_rejection( 41, _('No issue service found for this server/event') );
			return $receipt->dump( TRUE );
		}
		if ( $serviceData['status'] != 'active' ){
			$receipt->set_rejection( 42, _('Issue service is inactive') );
			return $receipt->dump( TRUE );
		}
		$ptcpId = $message->get('Participant-Id');
		$server = substr( strrchr( '@', $ptcpId ), 1 );
		if ( $server != $serviceData['id_provider'] ){
			$receipt->set_rejection( 43,
				sprintf( _('Only IDs from %s are entitled to request lot code for this event'),
				$serviceData['id_provider'] ) );
			return $receipt->dump( TRUE );
		}
		if ( ! $IssueServices->check_if_terms_are_matched( $ptcpId, $serviceData['terms'],
			$message->get('Participant-Attributes') )
		){
			$receipt->set_rejection( 44, _('Participant is not entitled to participate in the event') );
			return $receipt->dump( TRUE );
		}
		$receipt->set( 'Lot-Code', 'xxxxxxxxxxxxxxxx' );
		return $receipt->dump( TRUE );
	}


	static function handle_issue_service_block_order( $message, $receipt ){
		global $FileSystem, $IssueServices, $Timestamp;

		$eventId = $message->get('Event-Id');
		$server = $message->get('Event-Server') ?: 'localhost';
		$serviceId = $IssueServices->get_id( $server, $eventId );
		$serviceDir = $IssueServices->get_dir( $serviceId );
		$object = [ 'type' => 'issue_service', 'id' => $serviceId, 'dir' => $serviceDir ];
		$sender = $message->get('From');
		if ( ! ptcp_is_authorized( $sender, 'issue-service-block-order', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to order participant block') );
			return $receipt->dump( TRUE );
		}
		$serviceData = $IssueServices->get_data( $serviceId );
		if ( ! $serviceData ){
			$receipt->set_rejection( 41, _('Issue service does not exist') );
			return $receipt->dump( TRUE );
		}
		if ( $serviceData['status'] != 'active' ){
			$receipt->set_rejection( 42, _('Issue service is inactive') );
			return $receipt->dump( TRUE );
		}
		$ptcpId = $message->get('Participant-Id');
		$dir = "{$serviceDir}/participants/{$ptcpId}";
		$filename = $FileSystem->get_recent_filename( $dir );
		if ( ! $filename || substr( $filename, -12 ) != '-block-order' ){
			$receiptString = $receipt->dump( TRUE );
			$path = $dir . '/' . date( 'Ymd-His', $Timestamp ) . '-block-order';
			$FileSystem->put_contents( $path, $receiptString );
		}
		return $receiptString;
	}


	static function handle_issue_service_unblock_order( $message, $receipt ){
		global $FileSystem, $IssueServices, $Timestamp;

		$eventId = $message->get('Event-Id');
		$server = $message->get('Event-Server') ?: 'localhost';
		$serviceId = $IssueServices->get_id( $server, $eventId );
		$serviceDir = $IssueServices->get_dir( $serviceId );
		$object = [ 'type' => 'issue_service', 'id' => $serviceId, 'dir' => $serviceDir ];
		$sender = $message->get('From');
		if ( ! ptcp_is_authorized( $sender, 'issue-service-unblock-order', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to order participant unblock') );
			return $receipt->dump( TRUE );
		}
		$serviceData = $IssueServices->get_data( $serviceId );
		if ( ! $serviceData ){
			$receipt->set_rejection( 41, _('Issue service does not exist') );
			return $receipt->dump( TRUE );
		}
		if ( $serviceData['status'] != 'active' ){
			$receipt->set_rejection( 42, _('Issue service is inactive') );
			return $receipt->dump( TRUE );
		}
		$ptcpId = $message->get('Participant-Id');
		$receiptString = $receipt->dump( TRUE );
		$dir = "{$serviceDir}/participants/{$ptcpId}";
		$filename = $FileSystem->get_recent_filename( $dir );
		if ( $filename && substr( $filename, -12 ) == '-block-order' ){
			$path = $dir . '/' . date( 'Ymd-His', $Timestamp ) . '-unblock-order';
			$FileSystem->put_contents( $path, $receiptString );
		}
		return $receiptString;
	}

}

// end of file issue_services.messages.class.php

