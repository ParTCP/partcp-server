<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

error_reporting( E_ALL );
ini_set( 'display_errors', 1 );

$httpVersion = $_SERVER['SERVER_PROTOCOL'] ?? '1.1';

if ( empty( $_GET['event'] ) ){
	header( "HTTP/{$httpVersion} 400 Mandatory request parameter 'event' missing" );
	exit;
}

if ( strpos( $_GET['event'], '..' ) !== FALSE ){
	header( "HTTP/{$httpVersion} 400 Illegal characters in request parameter" );
	exit;
}

$BaseDir = __DIR__ . '/../..';
include "{$BaseDir}/config.php";
$DataDir = $Config['path_to_data'] ?? "{$BaseDir}/data";
require_once "{$BaseDir}/modules/core/counter.class.php";
$Counter = new ParTCP_Counter( $DataDir );
$Counter->set_base_dir('event_status');
$count = $Counter->get_value( $_GET['event'] );
$file = "{$DataDir}/event_status/{$_GET['event']}";
$interval = file_exists( $file ) ? file_get_contents( $file ) : 60;
header('Content-Type: text/plain');
echo "change_count: {$count}\npolling_interval: {$interval}\n";


// end of file events/poll.php

