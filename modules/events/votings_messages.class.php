<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Votings_Messages {

	static function handle_voting_definition( $message, $receipt ){
		global $Events, $FileSystem, $Timestamp, $Votings;

		$votingData = $message->get('Voting-Data');
		if ( empty( $votingData['name'] )
			|| empty( $votingData['title'] )
			|| empty( $votingData['type'] )
		){
			$receipt->set_rejection( 21, _('Incomplete voting data') );
			return $receipt->dump( TRUE );
		}
		$start = 0;
		if ( isset( $votingData['period_start'] )
			&& ( ! ( $start = strtotime( $votingData['period_start'] ) ) || $start < time() )
		){
			$receipt->set_rejection( 22, _('Invalid period start') );
			return $receipt->dump( TRUE );
		}
		if ( isset( $votingData['period_end'] )
			&& ( ! ( $end = strtotime( $votingData['period_end'] ) ) || $end < time() || $end <= $start )
		){
			$receipt->set_rejection( 23, _('Invalid period end') );
			return $receipt->dump( TRUE );
		}
		if ( ! empty( $votingData['comment_rules']['mandate_values'] ) ){
			$votingData['comment_rules']['mandate_values'] =
				array_filter( $votingData['comment_rules']['mandate_values'],
					function( $v ){ return $v !== ''; }
				);
		}
		$eventId = $message->get('Event-Id');
		$eventData = $Events->get_data( $eventId );
		if ( ! $eventData ){
			$receipt->set_rejection( 41, sprintf( _('Event %s does not exist'), $eventId ) );
			return $receipt->dump( TRUE );
		}
		if ( $eventData['status'] == 'finished' ){
			$receipt->set_rejection( 42, _('Event has been declared as finished') );
			return $receipt->dump( TRUE );
		}
		$eventDir = $Events->get_dir( $eventId );
		$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ];
		$sender = $message->get('From');
		if ( ! ptcp_is_authorized( $sender, 'voting-definition', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to define voting') );
			return $receipt->dump( TRUE );
		}
		$data = [
			'id' => strtolower( $votingData['name'] ),
			'event_id' => $eventId,
			'created_on' => date( 'c', $Timestamp ),
			'created_by' => $sender,
			'modified_on' => null,
			'modified_by' => null,
			'status' => 'idle',
		];
		$data = array_merge( $data, $Votings->purge_data( $votingData ) );
		$Votings->set_base_dir( $eventDir );
		$dir = $Votings->get_dir( $data['id'] );
		if ( $FileSystem->exists( $dir ) ){
			$receipt->set_rejection( 24, _('Voting with that name already exists') );
			return $receipt->dump( TRUE );
		}
		$success = $FileSystem->make_dir( $dir );
		if ( ! $success ){
			$receipt->set_failure( _('Could not create voting directory') );
			return $receipt->dump( TRUE );
		}
		$receipt->set( 'Message-Type', 'voting-definition-confirmation' );
		$receipt->set( 'Voting-Data', $data );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-voting-definition';
		$receiptString = $receipt->dump( TRUE );
		$FileSystem->put_contents( "{$dir}/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_voting_update_request( $message, $receipt ){
		global $Events, $FileSystem, $Timestamp, $Votings;
		$eventId = $message->get('Event-Id');
		$eventData = $Events->get_data( $eventId );
		if ( ! $eventData ){
			$receipt->set_rejection( 41, sprintf( _('Event %s does not exist'), $eventId ) );
			return $receipt->dump( TRUE );
		}
		if ( $eventData['status'] == 'finished' ){
			$receipt->set_rejection( 44, _('Event has been declared as finished') );
			return $receipt->dump( TRUE );
		}
		$eventDir = $Events->get_dir( $eventId );
		$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ];
		$sender = $message->get('From');
		if ( ! ptcp_is_authorized( $sender, 'voting-update-request', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to update voting') );
			return $receipt->dump( TRUE );
		}
		$Votings->set_base_dir( $eventDir );
		$votingId = $message->get('Voting-Id');
		$votingData = $Votings->get_data( $votingId );
		if ( ! $votingData ){
			$receipt->set_rejection( 42, sprintf( _('Voting %s does not exist'), $votingId ) );
			return $receipt->dump( TRUE );
		}
		$newData = $message->get('Voting-Data');
		if ( $votingData['status'] != 'idle' && array_keys( $newData ) != ['period_end'] ){
			$receipt->set_rejection( 43, sprintf( _('Voting %s cannot be updated anymore'), $votingId ) );
			return $receipt->dump( TRUE );
		}
		$start = 0;
		if ( ! empty( $newData['period_start'] )
			&& ( ! ( $start = strtotime( $newData['period_start'] ) ) || $start < time() )
		){
			$receipt->set_rejection( 22, _('Invalid period start') );
			return $receipt->dump( TRUE );
		}
		if ( ! empty( $newData['period_end'] )
			&& ( ! ( $end = strtotime( $newData['period_end'] ) ) || $end < time() || $end <= $start )
		){
			$receipt->set_rejection( 23, _('Invalid period end') );
			return $receipt->dump( TRUE );
		}
		if ( ! empty( $newData['comment_rules'] ) ){
			foreach ( $newData['comment_rules'] as $field => $value ){
				if ( is_array( $value ) ){
					$value = array_filter( $value, function( $v ){ return $v !== ''; } );
					$value = array_map( function( $v ){ return (int) $v; }, $value );
				}
				else {
					$value = (int) $value;
				}
				$newData['comment_rules'][ $field ] = $value;
			}
		}

		// Synchronize old and new attribute names (can be removed when all clients have been updated)
		if ( ! isset( $newData['short_description'] ) && isset( $newData['client_data']['short-description'] ) ){
			$newData['short_description'] = $newData['client_data']['short-description'];
		}
		if ( ! isset( $newData['description'] ) && isset( $newData['client_data']['description'] ) ){
			$newData['description'] = $newData['client_data']['description'];
		}
		if ( ! isset( $newData['link_url'] ) && isset( $newData['client_data']['link-url'] ) ){
			$newData['link_url'] = $newData['client_data']['link-url'];
		}
		if ( ! isset( $newData['client_data']['short-description'] ) && isset( $newData['short_description'] ) ){
			$newData['client_data']['short-description'] = $newData['short_description'];
		}
		if ( ! isset( $newData['client_data']['description'] ) && isset( $newData['description'] ) ){
			$newData['client_data']['description'] = $newData['description'];
		}
		if ( ! isset( $newData['client_data']['link-url'] ) && isset( $newData['link_url'] ) ){
			$newData['client_data']['link-url'] = $newData['link_url'];
		}

		ptcp_update_object( $votingData, $Votings->purge_data( $newData ) );
		$votingData['modified_on'] = date( 'c', $Timestamp );
		$votingData['modified_by'] = $sender;
		$receipt->set( 'Message-Type', 'voting-update-confirmation' );
		$receipt->set( 'Voting-Data', $votingData );
		$dir = $Votings->get_dir( $votingId );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-voting-update-request';
		$receiptString = $receipt->dump( TRUE );
		$FileSystem->put_contents( "{$dir}/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_voting_details_request( $message, $receipt ){
		global $Counter, $DataDir, $Events, $FileSystem, $Timestamp, $Votings;
		$eventId = $message->get('Event-Id');
		$eventDir = $Events->get_dir( $eventId );
		$votingId = $message->get('Voting-Id');
		$Votings->set_base_dir( $eventDir );
		$votingDir = $Votings->get_dir( $votingId );
		$votingData = $Votings->get_data( $votingId );
		if ( ! $votingData ){
			$receipt->set_rejection( 41, _('Voting does not exist') );
			return $receipt->dump( TRUE );
		}
		$sender = $message->get('From');
		if ( $message->get('Include-Admin-Info') && $sender ){
			$votingData['administrable'] = ptcp_is_authorized( $sender, 'event-update-request',
				[ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ] );
		}
		if ( $votingData['status'] == 'closed'
			&& $Timestamp > strtotime( $votingData['period_end'] )
			&& ( $key = hexdec( substr( md5( "{$eventId}/{$votingId}" ), -6 ) ) )
			&& ( $semId = sem_get( $key ) )
			&& sem_acquire( $semId, TRUE )
		){
			$votingData['voting_result'] = $Votings->count_votes( $votingId );
			$votingData['status'] = 'finished';
			$votingData['modified_on'] = date( 'c', $Timestamp );
			$votingData['modified_by'] = $sender;
			$receipt->set( 'Message-Type', 'voting-details' );
			$receipt->set( 'Voting-Data', $votingData );
			$fileName = date( 'Ymd-His', $Timestamp ) . '-surrogate-vote-count-request';
			$receiptString = $receipt->dump( TRUE );
			$FileSystem->put_contents( "{$votingDir}/{$fileName}", $receiptString );
			sem_release( $semId );
		}
		elseif ( ! empty( $votingData['confirm_segment_results'] ) ){
			$eventData = $Events->get_data( $eventId );
			$votingData['voting_result'] =
				$Votings->get_segment_results( $eventData, $votingData, $sender );
		}
		$Counter->set_base_dir( $votingDir );
		$receipt->set( 'Message-Type', 'voting-details' );
		$receipt->set( 'Voting-Data', $votingData );
		if ( $message->get('Include-Event-Data') ){
			$receipt->set( 'Event-Data', $Events->get_data( $eventId ) );
		}
		return $receipt->dump( TRUE );
	}


	static function handle_ballots_count_request( $message, $receipt ){
		global $Counter, $Events, $Votings;
		$eventDir = $Events->get_dir( $message->get('Event-Id') );
		$Votings->set_base_dir( $eventDir );
		$votingDir = $Votings->get_dir( $message->get('Voting-Id') );
		$Counter->set_base_dir( $votingDir );
		$receipt->set( 'Message-Type', 'ballots-count' );
		$receipt->set( 'Ballots-Count', $Counter->get_value('ballots') );
		return $receipt->dump( TRUE );
	}


	static function handle_voting_list_request( $message, $receipt ){
		global $Events, $Votings;
		$eventId = $message->get('Event-Id');
		$eventData = $Events->get_data( $eventId );
		$Votings->set_base_dir( $Events->get_dir( $eventId ) );
		$status = $message->get('Voting-Status') ?? 'open';
		$sortKey = $eventData['voting_sort_key'] ?? 'created_on';
		$sortDesc = ! empty( $eventData['voting_sort_order'] )
			&& $eventData['voting_sort_order'] == 'descending';
		$receipt->set( 'Votings', $Votings->get_list( $status, $sortKey, $sortDesc ) );
		$receipt->set( 'Message-Type', 'voting-list' );
		return $receipt->dump( TRUE );
	}


	static function handle_voting_start_declaration( $message, $receipt ){
		global $Counter, $DataDir, $Events, $FileSystem, $Timestamp, $Votings;

		$eventId = $message->get('Event-Id');
		$eventData = $Events->get_data( $eventId );
		if ( ! $eventData ){
			$receipt->set_rejection( 41, sprintf( _('Event %s does not exist'), $eventId ) );
			return $receipt->dump( TRUE );
		}
		if ( $eventData['status'] == 'finished' ){
			$receipt->set_rejection( 44, _('Event has been declared as finished') );
			return $receipt->dump( TRUE );
		}
		$eventDir = $Events->get_dir( $eventId );
		$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ];
		if ( ! ptcp_is_authorized( $message->get('From'),
			'voting-start-declaration', $object )
		){
			$receipt->set_rejection( 31, _('Sender is not authorized to declare voting start') );
			return $receipt->dump( TRUE );
		}
		$Votings->set_base_dir( $eventDir );
		$votingId = $message->get('Voting-Id');
		$votingData = $Votings->get_data( $votingId );
		if ( ! $votingData ){
			$receipt->set_rejection( 42, _('Voting does not exist') );
			return $receipt->dump( TRUE );
		}
		if ( $votingData['status'] != 'idle' ){
			$receipt->set_rejection( 43, _('Voting cannot be started twice') );
			return $receipt->dump( TRUE );
		}
		if ( $message->get('Polling-Interval') ){
			$file = "{$DataDir}/event_status/{$eventData['shortcode']}";
			file_put_contents( $file, $message->get('Polling-Interval') );
		}
		$Counter->set_base_dir('event_status');
		$Counter->increment( $eventData['shortcode'] );
		$votingData['period_start'] = date('c');
		$receipt->set( 'Message-Type', 'voting-update-confirmation' );
		$receipt->set( 'Voting-Data', $votingData );
		$receiptString = $receipt->dump( TRUE );
		$dir = $Votings->get_dir( $votingId, TRUE );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-voting-start-declaration';
		$FileSystem->put_contents( "{$dir}/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_voting_end_declaration( $message, $receipt ){
		global $Counter, $DataDir, $Events, $FileSystem, $Timestamp, $Votings;

		$eventId = $message->get('Event-Id');
		$eventData = $Events->get_data( $eventId );
		if ( ! $eventData ){
			$receipt->set_rejection( 41, sprintf( _('Event %s does not exist'), $eventId ) );
			return $receipt->dump( TRUE );
		}
		$eventDir = $Events->get_dir( $eventId );
		$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ];
		if ( ! ptcp_is_authorized( $message->get('From'), 'voting-end-declaration', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to declare voting end') );
			return $receipt->dump( TRUE );
		}
		$Votings->set_base_dir( $eventDir );
		$votingId = $message->get('Voting-Id');
		$votingData = $Votings->get_data( $votingId );
		if ( ! $votingData ){
			$receipt->set_rejection( 42, _('Voting does not exist') );
			return $receipt->dump( TRUE );
		}
		if ( $votingData['status'] == 'idle' ){
			$receipt->set_rejection( 43, _('Voting has not been started yet') );
			return $receipt->dump( TRUE );
		}
		elseif ( $votingData['status'] != 'open' ){
			$receipt->set_rejection( 44, _('Voting has already been closed') );
			return $receipt->dump( TRUE );
		}
		if ( $message->get('Polling-Interval') ){
			$file = "{$DataDir}/event_status/{$eventData['shortcode']}";
			file_put_contents( $file, $message->get('Polling-Interval') );
		}
		$Counter->set_base_dir('event_status');
		$Counter->increment( $eventData['shortcode'] );
		$votingData['period_end'] = date('c');
		$receipt->set( 'Message-Type', 'voting-update-confirmation' );
		$receipt->set( 'Voting-Data', $votingData );
		$dir = $Votings->get_dir( $votingId, TRUE );
		$receiptString = $receipt->dump( TRUE );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-voting-end-declaration';
		$FileSystem->put_contents( "{$dir}/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_ballot( $message, $receipt ){
		global $Config, $Counter, $DataDir, $Events, $FileSystem, $LocalId, $Votings;

		$originalMessage = $receipt->data['Original-Message'];
		$receipt->set('Original-Message');
		$receipt->set('Date');
		$eventId = $message->get('Event-Id');
		$eventDir = $Events->get_dir( $eventId );
		$eventData = $Events->get_data( $eventId );
		$anonymous = empty( $eventData['is_non_anonymous'] );
		$Votings->set_base_dir( $eventDir );
		$votingId = $message->get('Voting-Id');
		$votingDir = $Votings->get_dir( $votingId );
		$voting = $Votings->get_data( $votingId );
		if ( ! $voting ){
			$receipt->set_rejection( 41, sprintf( _('Unknown voting %s'), $votingId ) );
			return $receipt->dump( TRUE );
		}
		if ( $voting['status'] == 'idle' ){
			$receipt->set_rejection( 42, _('Voting has not been started yet') );
			return $receipt->dump( TRUE );
		}
		elseif ( in_array( $voting['status'], [ 'closed', 'finished' ] ) ){
			$receipt->set_rejection( 43, _('Voting has already been closed') );
			return $receipt->dump( TRUE );
		}
		if ( $anonymous && $message->get('Date') ){
			$receipt->set_rejection( 46, _('Date element is not allowed in anonymous ballots') );
			return $receipt->dump( TRUE );
		}
		// check if sender is entitled to vote
		$senderId = $message->get('From');
		list ( $ptcpId, $server ) = explode( '@', $senderId ) + [ '', '' ];
		if ( $anonymous ){
			if ( strpos( $ptcpId, '+' ) ){
				$ptcpId = substr( strrchr( $ptcpId, '+' ), 1 );
			}
			$LocalId->set_base_dir( $eventDir );
			$ptcpData = $LocalId->get_data( $ptcpId );
			$entitled =	$FileSystem->exists( "{$eventDir}/participants/{$ptcpId}" )
					&& empty( $ptcpData['invalidated'] )
					&& ( ! empty( $eventData['is_demo'] ) || $message->get_signature_status() );
		}
		else {
			$entitled = $FileSystem->exists( "{$eventDir}/participants/{$senderId}" );
		}
		if ( ! $entitled ){
			$receipt->set_rejection( 31, _('Sender is not entitled to participate in this voting') );
			return $receipt->dump( TRUE );
		}
		if ( ! empty( $voting['prerequisites'] ) ){
			foreach ( $voting['prerequisites'] as $prereq ){
				if ( ! $Votings->has_participant_voted( $prereq, $senderId ) ){
					$receipt->set_rejection( 44, _('Participant has not fulfilled prerequisites for this voting') );
					return $receipt->dump( TRUE );
				}
			}
		}
		// get ballots directory
		if ( $anonymous ){
			if ( strpos( $ptcpId, '+' ) ){
				list( $eventId, $ptcpId ) = explode( '+', $ptcpId );
			}
			$senderDir = $LocalId->get_dir( $ptcpId );
			$ballotsDir = "{$votingDir}/ballots/" . basename( strstr( $senderDir, '/' ) );
		}
		else {
			$senderDir = "{$eventDir}/participants/{$senderId}";
			$ballotsDir = "{$votingDir}/ballots/{$senderId}";
		}
		$ballotsCount = $FileSystem->count_items( $ballotsDir, 'ballot-*' );
		if ( ! empty( $voting['single_vote_only'] ) && $ballotsCount > 0 ){
			$receipt->set_rejection( 45, _('Repeated sending of ballot is not allowed in this voting.') );
			return $receipt->dump( TRUE );
		}
		// check options
		$options = array_column( $voting['options'], NULL, 'id' );
		$votes = array_column( $message->get('Votes'), NULL, 'id' );
		foreach ( $votes as $id => $vote ){
			if ( ! isset( $options[ $id ] ) ){
				$receipt->set_rejection( 91, sprintf( _('Ballot contains invalid option %s'), $id ) );
				return $receipt->dump( TRUE );
			}
			elseif ( $voting['type'] == 'mixed' ){
				if ( $options[ $id ]['type'] == 'text'
					&& ! empty( $votes[ $id ]['data'] )
					&& strlen( $votes[ $id ]['data'] ) > $options[ $id ]['max_length'] ?? 300
				){
					$receipt->set_rejection( 92, sprintf( _('Text of option %s exceeds maximum length'), $id ) );
					return $receipt->dump( TRUE );
				}
				elseif ( in_array( $options[ $id ]['type'], ['file','photo'] )
					&& ! empty( $votes[ $id ]['data'] )
				){
					if ( count( $votes[ $id ]['data'] ) > ( $options[ $id ]['max_count'] ?? 1 ) ){
						$receipt->set_rejection( 93, sprintf( _('Number of files in option %s exceeds limit'), $id ) );
						return $receipt->dump( TRUE );
					}
					elseif ( strlen( implode( $votes[ $id ]['data'] ) ) / 1024 / 1024 / 1.3
						> $options[ $id ]['max_size'] ?? 1
					){
						$receipt->set_rejection( 94, sprintf( _('Data of option %s exceeds allowed size'), $id ) );
						return $receipt->dump( TRUE );
					}
				}
				// TODO: Check MIME types
			}
		}
		if ( $voting['type'] == 'mixed' ){
			foreach ( $options as $id => $option ){
				if ( ! empty( $option['mandatory'] ) && ! isset( $votes[ $id ]['vote'] ) ){
					$receipt->set_rejection( 96, sprintf( _('Mandatory option %s is missing'), $id ) );
					return $receipt->dump( TRUE );
				}
			}
		}
		// check comments
		$acceptance = $voting['comment_rules']['acceptance'] ?? 0;
		$maxLength = $voting['comment_rules']['max_length'] ?? 300;
		$ballotComment = $message->get('Comment');
		$optionComments = array_filter( array_column( $votes, 'comment' ) );
		if ( ( ! $acceptance && ( $ballotComment || $optionComments ) )
			|| ( $acceptance == 1 && $optionComments )
			|| ( $acceptance == 2 && $ballotComment )
		){
			$receipt->set_rejection( 97, _('Ballot contains invalid comment') );
			return $receipt->dump( TRUE );
		}
		elseif ( ( $ballotComment && strlen( $ballotComment ) > $maxLength )
			|| ( $optionComments && max( array_map( 'strlen', $optionComments ) ) > $maxLength )
		){
			$receipt->set_rejection( 98, _('Comment is too long') );
			return $receipt->dump( TRUE );
		}
		// add sender to confirmation segment, if applicable
		if ( ! empty( $eventData['is_non_anonymous'] ) && ! empty( $eventData['segments'] )
			&& ! empty( $voting['confirm_segment_results'] )
		){
			$segmentId = $voting['confirm_segment_results'];
			$ptcpSegments = $Votings->get_participant_segments( $eventData, $senderId );
			//var_dump( $segmentId, $ptcpSegments );
			if ( ! empty( $ptcpSegments[ $segmentId ] ) ){
				$dir = "{$DataDir}/event_segments/{$eventData['shortcode']}/"
					. "{$segmentId}/{$ptcpSegments[ $segmentId ]}";
				@mkdir( $dir, 0755, TRUE );
				touch( "{$dir}/{$senderId}" );
			}
		}
		// store ballot
		$Counter->set_base_dir( $votingDir );
		$Counter->increment('ballots_received');
		$nextNumber = $ballotsCount + 1;
		if ( $nextNumber == 1 ){
			$Counter->increment('ballots');
		}
		$fileName = sprintf( 'ballot-%04d', $nextNumber );
		$receipt->set( 'Original-Message', $originalMessage );
		$FileSystem->put_contents( "{$ballotsDir}/{$fileName}", $receipt->dump( TRUE ), $anonymous );
		$receipt->set('Original-Message');
		$FileSystem->put_contents( "{$senderDir}/has_voted", '', $anonymous );
		$receipt->set( 'Event-Id', $eventId );
		$receipt->set( 'Voting-Id', $votingId );
		$Votings->create_interim_count_confirmation( $votingId, $receipt->localId,
			$Counter->get_value('ballots_received') );
		return $receipt->dump( TRUE );
	}


	static function handle_vote_count_request( $message, $receipt ){
		global $Events, $FileSystem, $Timestamp, $Votings;

		$eventId = $message->get('Event-Id');
		$eventData = $Events->get_data( $eventId );
		if ( ! $eventData ){
			$receipt->set_rejection( 41, sprintf( _('Event %s does not exist'), $eventId ) );
			return $receipt->dump( TRUE );
		}
		$eventDir = $Events->get_dir( $eventId );
		$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ];
		$sender = $message->get('From');
		if ( ! ptcp_is_authorized( $sender, 'vote-count-request', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to request vote count') );
			return $receipt->dump( TRUE );
		}
		$Votings->set_base_dir( $eventDir );
		$votingId = $message->get('Voting-Id');
		$votingData = $Votings->get_data( $votingId );
		if ( ! $votingData ){
			$receipt->set_rejection( 42, _('Voting does not exist') );
			return $receipt->dump( TRUE );
		}
		if ( $votingData['status'] == 'finished' ){
			$receipt->set_rejection( 43, _('Votes have already been counted') );
			return $receipt->dump( TRUE );
		}
		if ( $votingData['status'] != 'closed' ){
			$receipt->set_rejection( 44, _('Voting has not ended yet') );
			return $receipt->dump( TRUE );
		}
		$Counter->set_base_dir('event_status');
		$Counter->increment( $eventData['shortcode'] );
		$votingData['voting_result'] = $Votings->count_votes( $votingId );
		$votingData['modified_on'] = date( 'c', $Timestamp );
		$votingData['modified_by'] = $sender;
		$receipt->set( 'Message-Type', 'voting-details' );
		$receipt->set( 'Voting-Data', $votingData );
		$dir = $Votings->get_dir( $votingId );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-vote-count-request';
		$receiptString = $receipt->dump( TRUE );
		$FileSystem->put_contents( "{$dir}/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_voting_comments_request( $message, $receipt ){
		global $Events, $FileSystem, $Timestamp, $Votings;

		$eventId = $message->get('Event-Id');
		$eventData = $Events->get_data( $eventId );
		if ( ! $eventData ){
			$receipt->set_rejection( 41, sprintf( _('Event %s does not exist'), $eventId ) );
			return $receipt->dump( TRUE );
		}
		$eventDir = $Events->get_dir( $eventId );
		$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $eventDir ];
		$sender = $message->get('From');
		if ( ! ptcp_is_authorized( $sender, 'voting-comments-request', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to request voting comments') );
			return $receipt->dump( TRUE );
		}
		$Votings->set_base_dir( $eventDir );
		$votingId = $message->get('Voting-Id');
		$votingData = $Votings->get_data( $votingId );
		if ( ! $votingData ){
			$receipt->set_rejection( 42, _('Voting does not exist') );
			return $receipt->dump( TRUE );
		}
		if ( $votingData['status'] != 'finished' ){
			$receipt->set_rejection( 43, _('Voting has not ended yet') );
			return $receipt->dump( TRUE );
		}
		$comments = $Votings->get_comments( $votingId );
		$receipt->set( 'Message-Type', 'voting-comments' );
		$receipt->set( 'General-Comments', $comments['general'] );
		$receipt->set( 'Option-Comments', $comments['options'] );
		return $receipt->dump( TRUE );
	}
}

// end of file votings_messages.class.php

