<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Lots {

	public $fileSystem;
	public $baseDir;
	public $subdirDepth;
	public $subdirNameLength;
	public $config;


	public function __construct( $fileSystem, $config = [] ){
		$this->fileSystem = $fileSystem;
		$this->config = $config;
	}


	public function set_base_dir( $baseDir, $estimatedTotal = NULL ){
		if ( $estimatedTotal && function_exists('partcp_subdir_rules') ){
			$rules = partcp_subdir_rules( $estimatedTotal );
			$this->subdirDepth = $rules[0];
			$this->subdirNameLength = $rules[1];
		}
		$this->baseDir = trim( $baseDir, '/' );
	}


	public function get_dir( $id ){
		if ( ! empty( $this->subdirDepth ) ){
			$parts = str_split( md5( $id ), $this->subdirNameLength ?? 2 );
			$subdirs = implode( '/', array_slice( $parts, 0, $this->subdirDepth ) );
			$id = "{$subdirs}/{$id}";
		}
		return ( $this->baseDir ? "{$this->baseDir}/" : '' ) . "lots/{$id}";
	}

}

// end of file models/lots.class.php

