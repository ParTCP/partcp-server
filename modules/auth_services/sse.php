<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

include __DIR__ . '/../../initialize.php';
error_reporting( E_ALL );
ini_set( 'display_errors', 1 );
$httpVersion = $_SERVER['SERVER_PROTOCOL'] ?? '1.1';

if ( empty( $_GET['sid'] ) ){
	header( "HTTP/{$httpVersion} 400 Mandatory request parameter 'sid' missing" );
	exit;
}

session_id( $_GET['sid'] );
session_start();
session_write_close();

if ( empty( $_SESSION['state'] ) || empty( $_SESSION['auth_url'] ) ){
	header( "HTTP/{$httpVersion} 404 Invalid session" );
	exit;
}

$lastSessionData = $_SESSION;
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
header('Access-Control-Allow-Origin: *');
echo ':' . str_repeat( ' ', 2048 ) . "\n"; // 2 kB padding for IE
echo "retry: 2000\n";

while ( TRUE ) {
	if ( connection_aborted() ){
		break;
	}
	session_start();
	session_write_close();
	if ( $_SESSION != $lastSessionData ){
		echo 'data: ' . json_encode( $_SESSION ) . "\n\n";
		$lastSessionData = $_SESSION;
	}
	else {
		echo ": heartbeat\n\n";
	}
	ob_flush();
	flush();
	sleep( 2 );
}


// end of file auth_services/sse.php

