<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

require_once __DIR__ . '/auth_services.class.php';
$AuthServices = new ParTCP_Auth_Services( $FileSystem, $DataDir, $Partcp );

require_once __DIR__ . '/auth_services_messages.class.php';
ptcp_register_message_handler( 'ParTCP_Auth_Services_Messages', TRUE );

require_once __DIR__ . '/oidc_connections.class.php';
$OidcConnections = new ParTCP_Oidc_Connections( $FileSystem, $DataDir );

require_once __DIR__ . '/oidc_connections_messages.class.php';
ptcp_register_message_handler( 'ParTCP_Oidc_Connections_Messages', TRUE );

ini_set( 'session.use_cookies', 0 );

$MtdManager->register_dir( __DIR__ . '/mtd' );

if ( isset( $Server ) ){
	$Server->register_object( [ $AuthServices, 'get_list' ], 'Auth-Services' );
	$Server->register_object( [ $OidcConnections, 'get_list' ], 'Oidc-Connections' );
}

// end of file auth_services/init.php

