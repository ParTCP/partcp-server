<?php

/*
	A simple class for sending HTTP requests
	Copyright (C) 2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


class Simple_HTTP
{
	public $host;
	public $port;
	public $useSsl;
	public $useCurl = FALSE;	
	public $ignoreResponse = FALSE;
	public $lastRequest;
	public $lastResponse;

	public function __construct( $host, $useSsl = FALSE, $port = NULL ){
		$this->host = $host;
		$this->useSsl = $useSsl;
		$this->port = $port ? $port : ( $useSsl ? 443 : 80 );
	}


	public function send( $method, $path, $content = '', $contentType = '', $headers = array () ){
		if ( $content && ! $contentType ){
			$contentType = 'text/plain; charset=utf-8';
		}
		$method = strtoupper( $method );
		$path = ltrim( $path, '/' );
		$function = $this->useCurl ? 'send_via_curl' : 'send_via_socket';
		$response = call_user_func( [ $this, $function ], $method, $path, $content, $contentType, $headers );
		if ( ! $response ){
			return FALSE; // connection error
		}
		$this->lastResponse = $response;
		list ( $head, $body ) = explode( "\r\n\r\n", $response, 2 ) + ['',''];
		$status = (int) substr( $head, strpos( $head, ' ' ) + 1, 3 );
		if ( strpos( strtolower( $head ), "\r\ntransfer-encoding: chunked\r\n" ) ){
			$body = '';
			$markerPos = strlen( $head ) + 4;
			$markerLength = strpos( $response, "\r\n", $markerPos ) - $markerPos;
			$dataSize = hexdec( trim( substr( $response, $markerPos, $markerLength ) ) );
			while ( $dataSize > 0 ){
				$body .= substr( $response, $markerPos + $markerLength + 2, $dataSize );
				$markerPos =  $markerPos + $markerLength + 2 + $dataSize + 2;
				$markerLength = strpos( $response, "\r\n", $markerPos ) - $markerPos;
				$dataSize = hexdec( trim( substr( $response, $markerPos, $markerLength ) ) );
			}
		}
		return compact( 'status', 'head', 'body' );
	}


	private function send_via_socket( $method, $path, $content, $contentType, $headers ){
		$connection = fsockopen( ( $this->useSsl ? 'ssl://' : '' ) . $this->host, $this->port );
		if ( ! $connection ){
			return FALSE;
		}
		$request = $method . ' /' . $path . ' HTTP/1.1' . "\r\n"
			. 'Host: ' . $this->host . ( $this->port != 80 ? ':' . $this->port : '' ) . "\r\n" 
			. ( empty( $headers ) ? '' : implode( "\r\n", $headers ) . "\r\n" )
			. 'Connection: close' . "\r\n";
		if ( $content ){
			$request .= 'Content-Type: ' . $contentType . "\r\n"
						.  'Content-Length: ' . strlen( $content ) . "\r\n";
		}
		$request .= "\r\n" . $content;
		$this->lastRequest = $request;
		fwrite( $connection, $request );
		$response = '';
		if ( ! $this->ignoreResponse ){
			while ( $data = fgets( $connection ) ){
				$response .= $data;
			}
		}
		fclose( $connection );
		$this->lastResponse = $response;
		return $response;
	}


	private function send_via_curl( $method, $path, $content, $contentType, $headers ){
		$protocol = $this->useSsl ? 'https' : 'http';
		$curl = curl_init();
		curl_setopt( $curl, CURLOPT_URL, "{$protocol}://{$this->host}/{$path}" );
		curl_setopt( $curl, CURLOPT_FORBID_REUSE, TRUE );
		curl_setopt( $curl, CURLOPT_FRESH_CONNECT, TRUE );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, TRUE );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, FALSE );
		curl_setopt( $curl, CURLOPT_HEADER, TRUE );
		curl_setopt( $curl, CURLINFO_HEADER_OUT, TRUE );
		if ( $content ){
			$headers[] = 'Content-Type: ' . $contentType;
	      $headers[] = 'Content-Length: ' . strlen( $content );
			curl_setopt( $curl, CURLOPT_POSTFIELDS, $content );
		}
		if ( ! empty( $headers ) ){
			curl_setopt( $curl, CURLOPT_HTTPHEADER, $headers );
		}
		if ( $method == 'POST' ){
			curl_setopt( $curl, CURLOPT_POST, TRUE );
		}
		elseif ( $method != 'GET' ){
			curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, $method );
		}
		$response = curl_exec( $curl );
		$this->lastRequest = implode( "\n", curl_getinfo( $curl, CURLINFO_HEADER_OUT ) )
			. "\n{$content}";
		curl_close( $curl );
		return $response;
	}
}

// end of file simple_http.class.php

