<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/


if ( $_SERVER['REQUEST_METHOD'] == 'OPTIONS' ){
	die('ok');
}

include __DIR__ . '/../../../initialize.php';
error_reporting( E_ALL );
ini_set( 'display_errors', 1 );

$localHost = $_SERVER['HTTP_HOST'] ?? 'localhost';
$legacyApiUrl = "https://{$localHost}/modules/auth_services/legacy/api.php";
header('Content-Type: application/json');


// Initialize session

ini_set( 'session.use_cookies', 0 );
if ( ! empty( $_GET['sid'] ) ){
	session_id( $_GET['sid'] );
}
session_start();
$SessionId = session_id();


// Initialize auth service and OICD client

$serviceId = $Config['mod_auth_services']['default_service_id'] ?? NULL;
if ( ! $serviceId ){
	die( json_encode([ 'error' => 'Missing config parameter default_service_id' ]) );
}
$serviceData = $AuthServices->get_data( $serviceId );
if ( ! $serviceId ){
	die( json_encode([ 'error' => 'Authentication service could not be loaded.' ]) );
}
$Partcp->set_remote_id( $serviceData['target_id'] );
$oidcData = $OidcConnections->get_data( $serviceData['oidc_host'] );
if ( empty( $oidcData ) ){
	die( json_encode([ 'error' => 'OIDC connection data could not be loaded' ]) );
}
require_once dirname( __DIR__ ) . '/lib/oidc_client.class.php';
$oidc = new OIDC_Client( $oidcData['base_url'], $oidcData['client_id'],
	$oidcData['client_secret'] );


// Handle logout request

if ( isset( $_GET['logout'] ) ){
	unset( $_SESSION['username'] );
	unset( $_SESSION['profile'] );
	$uri = $oidc->get_logout_url();
	if ( isset( $serviceData['html_finish_logout'] ) ){
		$uri .= '?redirect_uri='
			. urlencode( "{$legacyApiUrl}?finish_logout&sid={$SessionId}" );
	}
	die( json_encode( [ 'redirect' => $uri	] ) );
}


// Handle finish_logout request

if ( isset( $_GET['finish_logout'] ) ){
	header('Content-Type: text/html');
	die( $serviceData['html_finish_logout'] ?? '' );
}


// Handle register and renewal request

if ( isset( $_GET['register'] ) || isset( $_GET['renewal'] ) ){
	$userId = $AuthServices->profile2id( $serviceData, $_SESSION['profile'] );
	if ( ! $userId ){
		die( json_encode( [ 'error' => 'User ID could not be retrieved' ] ) );
	}
	if ( isset( $_GET['register'] ) ){
		if ( ! isset( $_SESSION['consent_statement'] ) ){
			$response = $Partcp->get_server_details();
			$_SESSION['consent_statement'] = $response['server']['consent_statement'] ?? '';
			$_SESSION['privacy_notice'] = $response['server']['privacy_notice'] ?? '';
		}
		if ( ! empty( $_SESSION['consent_statement'] )
			&& empty( $_SESSION['consented'] )
		){
			die( json_encode( [
				'redirect' => "https://{$localHost}/modules/auth_services/legacy/"
					. "privacy.php?sid={$SessionId}",
				'url_register' => "{$legacyApiUrl}?register&sid={$SessionId}",
			]));
		}
	}
	if ( isset( $_GET['register'] ) ){
		$options = [];
		if ( isset( $_GET['register'] ) && ! empty( $_SESSION['consent_statement'] ) ){
			$options['consent_statement'] = $_SESSION['consent_statement'];
		}
		$credential = $Partcp->register_participant( $userId, $options );
	}
	else {
		$credential = $Partcp->permit_key_renewal( $userId );
	}
	if ( ! $credential ){
		die( json_encode([
			'error' => $Partcp->lastError,
			'debug_info' => $Partcp->lastRequest->data
		]) );
	}
	if ( ! empty( $serviceData['smtp_host'] )
		&& ! empty( $serviceData['email_notification_subject'] )
	){
		$userEmail = $AuthServices->profile2email( $serviceData, $_SESSION['profile'] );
		if ( $userEmail ){
			require_once __DIR__ . '/lib/smtp_mailer.class.php';
			$smtp = new SMTP_Mailer( $serviceData['smtp_host'], $serviceData['smtp_port'],
				$serviceData['smtp_username'], $serviceData['smtp_password'],
				$serviceData['smtp_security'] );
			$body = str_replace( '%id%', $id, $serviceData['email_notification_body'] );
			$smtp->mail( $email, $serviceData['email_notification_subject'], $body,
				$serviceData['email_notification_sender'] );
		}
	}
	die( json_encode( [ 'participant_id' => $userId, 'credential' => $credential ] ) );
}


// There's an active session - deliver status information

if ( isset( $_SESSION['profile'] ) ){
	$userId = $AuthServices->profile2id( $serviceData, $_SESSION['profile'] );
	$data = [
		'username' => $_SESSION['username'],
		'profile' => $_SESSION['profile'],
		'participant_id' => $userId,
		'url_status' => "{$legacyApiUrl}?sid={$SessionId}",
		'url_logout' => "{$legacyApiUrl}?logout&sid={$SessionId}",
		'url_register' => "{$legacyApiUrl}?register&sid={$SessionId}",
		'url_renewal' => "{$legacyApiUrl}?renewal&sid={$SessionId}",
	];
	$participant = $Partcp->get_participant_details( $userId );
	if ( ! $participant ){
		if ( strpos( $Partcp->lastError, '42' ) ){
			$data['participant_status'] = 'unregistered';
		}
		else {
			$data['participant_status'] = 'unknown';
			$data['error'] = $Partcp->lastError;
			$data['debug_info'] = $Partcp->lastRequest;
		}
	}
	else {
		if ( empty( $participant['public_key'] ) ){
			$data['participant_status'] = 'incomplete';
		}
		else {
			$data['participant_status'] = 'complete';
			$data['public_key'] = $participant['public_key'];
		}
	}
	die( json_encode( $data ) );
}


// Start authorization process (if not started before)

if ( empty( $_SESSION['state'] ) ){
	die( json_encode([
		'redirect' => $oidc->get_authorization_url(),
		'url_status' => "{$legacyApiUrl}?sid={$SessionId}",
	]) );
}


// Nothing to do - return URL for requesting status

die( json_encode( [ 'url_status' => "{$legacyApiUrl}?sid={$SessionId}" ] ) );


// end of file legacy/api.php

