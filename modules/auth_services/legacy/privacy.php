<html>
<head>
	<meta charset="utf-8">
	<title>Privacy Statement</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!--
		This file is part of the ParTCP Server project
		Copyright (C) 2024 Martin Wandelt

		This program is free software: you can redistribute it and/or modify
		it under the terms of the GNU Affero General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
		(at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU Affero General Public License for more details.

		You should have received a copy of the GNU Affero General Public License
		along with this program.  If not, see <https://www.gnu.org/licenses/>
	-->
</head>
<?php
	include __DIR__ . '/../../../initialize.php';
	error_reporting( E_ALL );
	ini_set( 'display_errors', 1 );

	if ( ! empty( $_REQUEST['sid'] ) ){
		session_id( $_REQUEST['sid'] );
	}
	session_start();
	if ( empty( $_SESSION['consent_statement'] ) ){
		die('Invalid session');
	}

	$localHost = $_SERVER['HTTP_HOST'] ?? 'localhost';
	$legacyApiUrl = "https://{$localHost}/modules/auth_services/legacy/api.php";
	if ( ! empty( $_REQUEST['consented'] ) && ! empty( $_REQUEST['consent_statement'] )
		&& $_REQUEST['consent_statement'] == $_SESSION['consent_statement']
	){
		$_SESSION['consented'] = TRUE;
	}
	if ( ! empty( $_SESSION['consented'] ) ){
		echo '<h1>Thank you!</h1>';
		exit;
	}
?>
<body>
	<script>
		function check_confirmation(){
			document.getElementById('btnProceed').disabled = ! document.getElementById('fldConfirm').checked;
		}
	</script>
	<div class="container">
		<h1 class="mt-5 mb-5">Privacy Statement</h1>
		<form name="form" action="" method="POST">
			<input type="hidden" name="consent_statement" value="<?= htmlspecialchars( $_SESSION['consent_statement'] ) ?>">
			<div class="form-group">
				<textarea class="form-control" cols="80" rows="20" readonly><?= $_SESSION['privacy_notice'] ?? '' ?></textarea>
			</div>
			<div class="form-group form-check">
				<input class="form-check-input" id="fldConfirm" name="consented" value="1" type="checkbox" onchange="check_confirmation()">
				<label class="form-check-label " for="fldConfirm">
					<?= $_SESSION['consent_statement'] ?>
				</label>
			</div>
			<button id="btnProceed" class="btn btn-primary" type="submit" disabled>
				<?= _('Proceed') ?>
			</button>
		</form>
	</div>
</body>
</html>

