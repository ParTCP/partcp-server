<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Oidc_Connections {
	
	public $fileSystem;
	public $dataDir;


	public function __construct( $fileSystem, $dataDir ){
		$this->fileSystem = $fileSystem;
		$this->dataDir = $dataDir;
	}


	public function get_dir( $oidcHost ){
		return "oidc/{$oidcHost}";
	}


	public function get_list(){
		$dir = 'oidc';
		if ( ! $this->fileSystem->exists( $dir ) ){
			return [];
		}
		$list = $this->fileSystem->get_listing( $dir );
		$oidcConnections = [];
		foreach ( $list as $oidcHost ){
			if ( $data = $this->get_data( $oidcHost ) ){
				unset( $data['client_secret'] );
				$oidcConnections[] = $data;
			}
		}
		return $oidcConnections;
	}


	public function get_data( $oidcHost ){
		$dir = $this->get_dir( $oidcHost ) . '/_def';
		$msg = $this->fileSystem->get_recent_contents( $dir, '[0-9]*-{update,definition}*' );
		if ( empty( $msg ) ){
			return FALSE;
		}
		$receipt = yaml_parse( $msg );
		if ( empty( $receipt['Oidc-Connection-Data'] ) ){
			return FALSE;
		}
		$data = $receipt['Oidc-Connection-Data'];
		if ( is_string( $data ) ){
			$data = json_decode( ptcp_local_decrypt( $data ), TRUE );
		}
		return $data;
	}


	public function purge_data( $data ){
		$validKeys = [
			'base_url' => 0,
			'client_id' => 0,
			'client_secret' => 0,
		];
		return array_intersect_key( $data, $validKeys );
	}


	public function update_endpoints( &$data ){
		require_once __DIR__ . '/lib/oidc_client.class.php';
		$oidc = new Oidc_Client( $data['base_url'], $data['client_id'],
			$data['client_secret'] );
		$metaData = $oidc->get_meta_data();
		if ( ! $metaData ){
			return FALSE;
		}
		$data['authorization_endpoint'] = $metaData['authorization_endpoint'];
		$data['token_endpoint'] = $metaData['token_endpoint'];
		$data['userinfo_endpoint'] = $metaData['userinfo_endpoint'];
		return TRUE;
	}


}

// end of file oidc_connections.class.php

