<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Oidc_Connections_Messages {
	
	static function handle_oidc_connection_definition( $message, $receipt ){
		global $OidcConnections, $FileSystem, $Timestamp;

		$sender = $message->get('From');
		$object = [ 'type' => 'server', 'id' => '', 'dir' => '/' ];
		if ( ! ptcp_is_authorized( $sender, 'oidc-connection-definition', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to define OIDC connections') );
			return $receipt->dump( TRUE );
		}
		$data = $message->get('Oidc-Connection-Data');
		if ( empty( $data['base_url'] ) || empty( $data['client_id'] )
			|| empty( $data['client_secret'] )
		){
			$receipt->set_rejection( 21, _('Incomplete OIDC connection data') );
			return $receipt->dump( TRUE );
		}
		$oidcHost = parse_url( $data['base_url'], PHP_URL_HOST );
		if ( ! $oidcHost ){
			$receipt->set_rejection( 22, _('OIDC base URL is invalid') );
			return $receipt->dump( TRUE );
		}
		$dir = $OidcConnections->get_dir( $oidcHost );
		if ( $FileSystem->exists( $dir ) ){
			$receipt->set_rejection( 41, _('OIDC connection is already defined for this host') );
			return $receipt->dump( TRUE );
		}
		$data = array_merge( [
			'id' => $oidcHost,
			'created_on' => date( 'c', $Timestamp ),
			'created_by' => $sender,
			'modified_on' => null,
			'modified_by' => null,
		], $data );
		if ( ! $OidcConnections->update_endpoints( $data ) ){
			$receipt->set_rejection( 91, _('Could not retrieve OIDC host endpoints') );
			return $receipt->dump( TRUE );
		}
		$success = $FileSystem->make_dir( $dir );
		if ( ! $success ){
			$receipt->set_failure( _('Could not create OIDC connection directory') . ': '
				. $FileSystem->lastError );
			return $receipt->dump( TRUE );
		}

		$fileName = date( 'Ymd-His', $Timestamp ) . '-oidc-connection-definition';
		$receipt->set( 'Message-Type', 'oidc-connection-details' );
		$receipt->set( 'Oidc-Connection-Data', ptcp_local_encrypt( $data ) );
		$FileSystem->put_contents( "{$dir}/_def/{$fileName}", $receipt->dump( TRUE ) );
		unset( $data['client_secret'] );
		$receipt->set( 'Oidc-Connection-Data', $data );
		return $receipt->dump( TRUE );
	}


	static function handle_oidc_connection_update_request( $message, $receipt ){
		global $OidcConnections, $FileSystem, $Timestamp;

		$id = $message->get('Oidc-Connection-Id');
		$data = $OidcConnections->get_data( $id );
		if ( ! $data ){
			$receipt->set_rejection( 41, _('OIDC connection not found') );
			return $receipt->dump( TRUE );
		}
		$dir = $OidcConnections->get_dir( $id );
		$object = [ 'type' => 'server', 'id' => '', 'dir' => '/' ];
		$sender = $message->get('From');
		if ( ! ptcp_is_authorized( $sender, 'oidc-connection-update-request', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to update OIDC connections') );
			return $receipt->dump( TRUE );
		}
		if ( ! empty( $data['base_url'] ) ){
			$oidcHost = parse_url( $data['base_url'], PHP_URL_HOST );
			if ( ! $oidcHost ){
				$receipt->set_rejection( 21, _('OIDC base URL is invalid') );
				return $receipt->dump( TRUE );
			}
		}
		$newData = $message->get('Oidc-Connection-Data');
		ptcp_update_object( $data, $OidcConnections->purge_data( $newData ) );
		$data['modified_on'] = date( 'c', $Timestamp );
		$data['modified_by'] = $sender;
		if ( ! $OidcConnections->update_endpoints( $data ) ){
			$receipt->set_rejection( 91, _('Could not retrieve OIDC host endpoints') );
			return $receipt->dump( TRUE );
		}
		$fileName = date( 'Ymd-His', $Timestamp ) . '-oidc-connection-update-request';
		$receipt->set( 'Message-Type', 'oidc-connection-details' );
		$receipt->set( 'Oidc-Connection-Data', ptcp_local_encrypt( $data ) );
		$FileSystem->put_contents( "{$dir}/_def/{$fileName}", $receipt->dump( TRUE ) );
		unset( $data['client_secret'] );
		$receipt->set( 'Oidc-Connection-Data', $data );
		return $receipt->dump( TRUE );
	}


	static function handle_oidc_connection_list_request( $message, $receipt ){
		global $OidcConnections;
		$list = $OidcConnections->get_list();
		$receipt->set( 'Oidc-Connections', $list );
		return $receipt->dump( TRUE );
	}


	static function handle_oidc_connection_details_request( $message, $receipt ){
		global $OidcConnections;
		$id = $message->get('Oidc-Connection-Id');
		$dir = $OidcConnections->get_dir( $id );
		$object = [ 'type' => 'oidc_connections', 'id' => $id, 'dir' => $dir ];
		$sender = $message->get('From');
		if ( ! ptcp_is_authorized( $sender, 'oidc-connection-details-request', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to request OIDC connection details') );
			return $receipt->dump( TRUE );
		}
		$data = $OidcConnections->get_data( $message->get('Oidc-Connection-Id') );
		if ( ! $data ){
			$receipt->set_rejection( 41, _('OIDC connection not found') );
			return $receipt->dump( TRUE );
		}
		unset( $data['client_secret'] );
		$receipt->set( 'Message-Type', 'oidc-connections-details' );
		$receipt->set( 'Oidc-Connection-Data', $data, TRUE );
		return $receipt->dump( TRUE );
	}


}

// end of file oidc_connections.messages.class.php

