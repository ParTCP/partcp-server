<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Auth_Services_Messages {
	
	static function handle_auth_service_definition( $message, $receipt ){
		global $AuthServices, $FileSystem, $OidcConnections, $Partcp, $Timestamp;

		$data = $message->get('Auth-Service-Data');
		if ( empty( $data['auth_type'] ) || empty( $data['target_id'] )
			|| empty( $data['p2id_format'] ) || empty( $data['p2id_parameters'] )
		){
			$receipt->set_rejection( 21, _('Missing attributes') );
			return $receipt->dump( TRUE );
		}
		if ( $data['auth_type'] == 'oidc' ){
			if ( empty( $data['oidc_host'] ) ){
				$receipt->set_rejection( 23, _('Missing OIDC host') );
				return $receipt->dump( TRUE );
			}
			$oidc = $OidcConnections->get_data( $data['oidc_host'] );
			if ( ! $oidc ){
				$receipt->set_rejection( 24, _('Invalid OIDC host') );
				return $receipt->dump( TRUE );
			}
		}
		else {
			$receipt->set_rejection( 22, _('Invalid authentication type') );
			return $receipt->dump( TRUE );
		}
		$sender = $message->get('From');
		$object = [ 'type' => 'server', 'id' => '', 'dir' => '/' ];
		if ( ! ptcp_is_authorized( $sender, 'auth-service-definition', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to define authentication services') );
			return $receipt->dump( TRUE );
		}
		$dir = $AuthServices->get_dir( $data['target_id'] );
		if ( $FileSystem->exists( $dir ) ){
			$receipt->set_rejection( 41, _('Authentication service is already defined for this target') );
			return $receipt->dump( TRUE );
		}
		$response = $AuthServices->check_target( $data );
		if ( $response == -1 ){
			$receipt->set_rejection( 22, _('Invalid target') );
			return $receipt->dump( TRUE );
		}
		if ( $response == -2 ){
			$receipt->set_rejection( 42, _('Could not connect to target') . ": {$Partcp->lastError}" );
			return $receipt->dump( TRUE );
		}
		if ( $response == -3 ){
			$receipt->set_rejection( 43, _('Missing privilege for administering target') );
			return $receipt->dump( TRUE );
		}
		$success = $FileSystem->make_dir( $dir );
		if ( ! $success ){
			$receipt->set_failure( _('Could not create auth service directory') . ': '
				. $FileSystem->lastError );
			return $receipt->dump( TRUE );
		}

		$data = array_merge( [
			'id' => $data['target_id'],
			'created_on' => date( 'c', $Timestamp ),
			'created_by' => $sender,
			'modified_on' => null,
			'modified_by' => null,
		], $data );
		ptcp_update_object( $data, $AuthServices->purge_data(
			$message->get('Auth-Service-Data') ) );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-auth-service-definition';
		$receipt->set( 'Message-Type', 'auth-service-details' );
		$receipt->set( 'Auth-Service-Data', $data );
		$receiptString = $receipt->dump( TRUE );
		$FileSystem->put_contents( "{$dir}/_def/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_auth_service_update_request( $message, $receipt ){
		global $AuthServices, $FileSystem, $OidcConnections, $Timestamp;

		$id = $message->get('Auth-Service-Id');
		$data = $AuthServices->get_data( $id );
		if ( ! $data ){
			$receipt->set_rejection( 41, _('Auth service does not exist') );
			return $receipt->dump( TRUE );
		}
		$dir = $AuthServices->get_dir( $id );
		$object = [ 'type' => 'server', 'id' => '', 'dir' => '/' ];
		$sender = $message->get('From');
		if ( ! ptcp_is_authorized( $sender, 'auth-service-update-request', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to update auth service') );
			return $receipt->dump( TRUE );
		}

		$newData = $message->get('Auth-Service-Data');
		if ( ! empty( $newData['auth_type'] ) ){
			if ( $newData['auth_type'] == 'oidc' ){
				if ( empty( $newData['oidc_host'] ) ){
					if ( empty( $data['oidc_host'] ) ){
						$receipt->set_rejection( 22, _('Missing OIDC host') );
						return $receipt->dump( TRUE );
					}
					unset( $newData['oidc_host'] );
				}
				else {
					$oidc = $OidcConnections->get_data( $newData['oidc_host'] );
					if ( ! $oidc ){
						$receipt->set_rejection( 23, _('Invalid OIDC host') );
						return $receipt->dump( TRUE );
					}
				}
			}
			else {
				$receipt->set_rejection( 21, _('Invalid authentication type') );
				return $receipt->dump( TRUE );
			}
		}
		if ( empty( $data['p2id_format'] ) ){
			unset( $data['p2id_format'] );
		}
		if ( empty( $data['p2id_parameters'] ) ){
			unset( $data['p2id_parameters'] );
		}
		ptcp_update_object( $data, $AuthServices->purge_data( $newData ) );
		$data['modified_on'] = date( 'c', $Timestamp );
		$data['modified_by'] = $sender;
		$receipt->set( 'Message-Type', 'auth-service-details' );
		$receipt->set( 'Auth-Service-Data', $data );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-auth-service-update-request';
		$receiptString = $receipt->dump( TRUE );
		$FileSystem->put_contents( "{$dir}/_def/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_auth_service_list_request( $message, $receipt ){
		global $AuthServices;
		$list = $AuthServices->get_list();
		$receipt->set( 'Auth-Services', $list );
		return $receipt->dump( TRUE );
	}


	static function handle_auth_service_details_request( $message, $receipt ){
		global $AuthServices;
		$data = $AuthServices->get_data( $message->get('Auth-Service-Id') );
		if ( ! $data ){
			$receipt->set_rejection( 41, _('Auth service does not exist') );
			return $receipt->dump( TRUE );
		}
		$receipt->set( 'Message-Type', 'auth-service-details' );
		$receipt->set( 'Auth-Service-Data', $data );
		return $receipt->dump( TRUE );
	}


	static function handle_auth_session_start_request( $message, $receipt ){
		global $AuthServices, $OidcConnections;
		$serviceId = $message->get('Auth-Service-Id');
		$authService = $AuthServices->get_data( $serviceId );
		if ( ! $authService ){
			$receipt->set_rejection( 41, _('No authentication service defined for this target') );
			return $receipt->dump( TRUE );
		}
		if ( $authService['auth_type'] != 'oidc' ){
			$receipt->set_rejection( 42, _('Authentication service does not support sessions') );
			return $receipt->dump( TRUE );
		}
		session_start();
		$oidcData = $OidcConnections->get_data( $authService['oidc_host'] );
		require_once __DIR__ . '/lib/oidc_client.class.php';
		$oidc = new OIDC_Client( $oidcData['base_url'], $oidcData['client_id'],
			$oidcData['client_secret'] );
		$authUrl = $oidc->get_authorization_url();
		if ( ! $authUrl ){
			$receipt->set_rejection( 91, _('OIDC error') . ": {$oidc->lastError}" );
			return $receipt->dump( TRUE );
		}
		$_SESSION['id'] = session_id();
		$_SESSION['target_id'] = $authService['target_id'];
		$_SESSION['auth_url'] = $authUrl;
		$_SESSION['sse_url'] = __DIR__ . "/sse.php?sid={$_SESSION['id']}";
		$receipt->set( 'Message-Type', 'auth-session-details' );
		$receipt->set( 'Oidc-Session-Data', $_SESSION );
		return $receipt->dump( TRUE );
	}


	static function handle_auth_session_details_request( $message, $receipt ){
		session_id( $message->get('Session-Id') );
		session_start();
		if ( ! $_SESSION['id'] ){
			$receipt->set_rejection( 41, _('Authentication session does not exist') );
			return $receipt->dump( TRUE );
		}
		$receipt->set( 'Message-Type', 'auth-session-details' );
		$receipt->set( 'Auth-Session-Data', $_SESSION );
		return $receipt->dump( TRUE );
	}


	static function handle_auth_session_action_request( $message, $receipt ){
		global $AuthServices, $Partcp;

		session_id( $message->get('Session-Id') );
		session_start();
		if ( ! $_SESSION['id'] ){
			$receipt->set_rejection( 41, _('Authentication session does not exist') );
			return $receipt->dump( TRUE );
		}
		if ( empty( $_SESSION['profile'] ) ){
			$receipt->set_rejection( 42, _('Authentication has not been fulfilled yet') );
			return $receipt->dump( TRUE );
		}
		$action = $message->get('Action');
		if ( ! in_array( $action, [ 'registration', 'key_renewal' ] ) ){
			$receipt->set_rejection( 22, sprintf( _('Action %s is not supported') ),
				$action );
			return $receipt->dump( TRUE );
		}
		$authService = $AuthServices->get_data( $_SESSION['target_id'] );
		$result = $AuthServices->perform_action( $action, $authService,
			$_SESSION['profile'] );
		if ( $result['status'] == 'reject' ){
			$receipt->set_rejection( 91, $result['result'] );
			return $receipt->dump( TRUE );
		}
		if ( $result['status'] == 'failure' ){
			$receipt->set_rejection( 92, $result['result'] );
			return $receipt->dump( TRUE );
		}
		$receipt->set( 'Message-Type', 'auth-session-action-confirmation' );
		$receipt->set( 'Action-Result', $result['result'], TRUE );
		return $receipt->dump( TRUE );
	}

}

// end of file auth_services_messages.class.php

