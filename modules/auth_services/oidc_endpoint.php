<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

include __DIR__ . '/../../initialize.php';
error_reporting( E_ALL );
ini_set( 'display_errors', 1 );

// TODO: Replace JSON responses by log entries

header('Content-Type: application/json');

if ( ! isset( $_GET['code'] ) ){
	die( json_encode( [
		'error' => "Missing code parameter"
	]));
}

if ( isset( $_GET['error'] ) ){
	die( json_encode( [
		'error' => "Authorization server returned an error: {$_GET['error']}"
	]));
}

if ( ! empty( $_GET['sid'] ) ){
	session_id( $_GET['sid'] );
}

session_start();

if ( empty( $_SESSION['state'] ) || empty( $_SESSION['auth_url'] ) ){
	die( json_encode( [
		'error' => 'Invalid session'
	]));
}

if ( $_SESSION['state'] != $_GET['state'] ){
	die( json_encode( [
		'error' => 'Authorization server returned an invalid state parameter'
	]));
}

$oidcHost = parse_url( $_SESSION['auth_url'], PHP_URL_HOST );
$oidcData = $OidcConnections->get_data( $oidcHost );
if ( empty( $oidcData ) ){
	die( json_encode( [
		'error' => "Could not get data for OIDC host {$oidcHost}"
	]));
}

require_once __DIR__ . '/lib/oidc_client.class.php';
$oidc = new OIDC_Client( $oidcData['base_url'], $oidcData['client_id'],
	$oidcData['client_secret'] );
$accessToken = $oidc->get_access_token( $_GET['code'] );
if ( ! $accessToken ){
	die( json_encode( [
		'error' => "Could not retrieve access token: {$oidc->lastError}"
	]));
}

$userInfo = $oidc->get_user_info( $accessToken );
if ( empty( $userInfo['sub'] ) ){
	die( json_encode( [
		'error' => "Could not retrieve user info: {$oidc->lastError}"
	]));
}
$_SESSION['sub'] = $userInfo['sub'];
$_SESSION['username'] = $userInfo['preferred_username'];
$_SESSION['profile'] = $userInfo;


// end of file oidc_endpoint.php

