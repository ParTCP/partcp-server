<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Auth_Services {
	
	public $fileSystem;
	public $dataDir;
	public $partcp;  // ParTCP object for sending messages


	public function __construct( $fileSystem, $dataDir, &$partcp ){
		$this->fileSystem = $fileSystem;
		$this->dataDir = $dataDir;
		$this->partcp = $partcp;
	}


	public function get_dir( $serviceId ){
		return "auth/{$serviceId}";
	}


	public function get_data( $serviceId ){
		$dir = $this->get_dir( $serviceId ) . '/_def';
		$msg = $this->fileSystem->get_recent_contents( $dir, '[0-9]*-{update,definition}*' );
		if ( empty( $msg ) ){
			return FALSE;
		}
		$receipt = yaml_parse( $msg );
		if ( empty( $receipt['Auth-Service-Data'] ) ){
			return FALSE;
		}
		$data = $receipt['Auth-Service-Data'];
		return $data;
	}


	public function get_list(){
		if ( ! $this->fileSystem->exists('auth') ){
			return [];
		}
		$services = $this->fileSystem->get_listing('auth');
		return $services;
	}


	public function purge_data( $data ){
		$validKeys = [
			'auth_type' => 0,
			'oidc_host' => 0,
			'p2id_format' => 0,
			'p2id_parameters' => 0,
		];
		return array_intersect_key( $data, $validKeys );
	}


	public function check_target( $serviceData ){
		$this->partcp->set_remote_id( $serviceData['target_id'] );
		$msg = [ 'Message-Type' => 'server-details-request' ];
		$response = $this->partcp->send_message( $msg );
		if ( ! $response ){
			return -2;
		}
		if ( empty( $response->get( 'Server-Data' )['administrable'] ) ){
			return -3;
		}
		return 1;
	}


	public function profile2id( $serviceData, $userData ){
		if ( empty( $serviceData['p2id_parameters'] ) ){
			return FALSE;
		}
		foreach ( $serviceData['p2id_parameters'] as $param ){
			if ( ! isset( $userData[ $param ] ) ){
				return FALSE;
			}
			$data[] = $userData[ $param ];
		}
		return sprintf( $serviceData['p2id_format'], ...$data );
	}


	public function profile2email( $serviceData, $userData ){
		if ( empty( $serviceData['p2email_parameters'] ) ){
			return FALSE;
		}
		foreach ( $serviceData['p2email_parameters'] as $param ){
			if ( ! isset( $userData[ $param ] ) ){
				return FALSE;
			}
			$data[] = $userData[ $param ];
		}
		return sprintf( $serviceData['p2email_format'], ...$data );
	}


	public function get_allowed_actions( $serviceData, $userData ){
		if ( ! $this->profile2id( $serviceData, $userData ) ){
			return [];
		}
		return [ 'registration', 'key_renewal' ];
	}


	public function check_entitlement( $action, $serviceData, $userData ){
		return in_array( $action, $this->get_allowed_actions( $serviceData, $userData ) );
	}


	public function perform_action( $action, $serviceData, $userData ){
		$funcName = "perform_action_{$action}";
		if ( ! method_exists( $this, $funcName ) ){
			return [ 'status' => 'reject', 'result' => 'Unknown action' ];
		}
		$ptcpId = $this->profile2id( $serviceData, $userData );
		if ( ! $ptcpId ){
			return [ 'status' => 'reject', 'result' => 'Cannot calculate ID due to missing user attribute' ];
		}
		return call_user_func( [ $this, $funcName ], $ptcpId, $serviceData, $userData );
	}


	private function perform_action_registration( $ptcpId, $serviceData, $userData ){
		$this->partcp->set_remote_id( $serviceData['target_id'] );
		if ( $consent = $message->get('Consent-Statement') ){
			$options['Consent-Statement'] = $consent;
		}
		$credential = $this->partcp->register_participant( $ptcpId, $options ?? [] );
		if ( ! $credential ){
			return [ 'status' => 'failure',
				'result' => _('Registration failed') . ": {$this->partcp->lastError}" ];
		}
		return [ 'status' => 'success', 'result' => $credential ];
	}


	private function perform_action_key_renewal( $ptcpId, $serviceData, $userData ){
		$this->partcp->set_remote_id( $serviceData['target_id'] );
		$credential = $this->partcp->permit_key_renewal( $ptcpId );
		if ( ! $credential ){
			return [ 'status' => 'failure','result' => _('Request for key renewal failed')
				. ": {$this->partcp->lastError}" ];
		}
		return [ 'status' => 'success', 'result' => $credential ];
	}

}

// end of file auth_service.class.php

