<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2025 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

require_once __DIR__ . '/hooks.class.php';
$Hooks = new ParTCP_Hooks( $FileSystem, $DataDir );
$FileSystem->register_hook( [ $Hooks, 'trigger_hooks' ] );


// end of file hooks/init.php

