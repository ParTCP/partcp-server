<?php

function post_request( $url, $params ){
	$content = http_build_query( $params );
	$fp = fopen( $url, 'r', FALSE,
		stream_context_create([
			'http' => [
				'header'  => [
					'Content-type: application/x-www-form-urlencoded',
					'Content-Length: ' . strlen( $content )
				],
				'method'  => 'POST',
				'content' => $content
			]
		])
	);
	if ( $fp === FALSE ){
		return FALSE;
	}
	$result = stream_get_contents( $fp );
	fclose( $fp );
	return $result;
}

### Main script ###

error_reporting( E_ALL );
ini_set( 'display_errors', 1 );
echo "Processor triggered {$argv[1]}\n";

if ( empty( $argv[1] ) || ! is_dir( $argv[1] ) ){
	die( "Specify queue directory as parameter.\n" );
}

chdir( $argv[1] );
$dh = opendir('.');

if ( ! $dh ){
	die( "Could not open queue directory for reading.\n" );
}

while ( ( $dir = readdir( $dh ) ) !== FALSE ){
	if ( $dir[0] == '.' || ! is_dir( $dir )
		|| ! file_exists( $dir . '/data.yaml' )
	){
		continue;
	}
	$data = yaml_parse( file_get_contents( $dir . '/data.yaml' ) );

	if ( ! mkdir( $dir . '/lock' ) ){
		continue; // another process is handling this item
	}

	if ( empty( $data['url'] ) || empty( $data['path'] )
		|| empty( $data['content'] ) || empty( $data['hash'] )
	){
		unlink( $dir . '/lock' );
		echo "Missing data in $dir/data.yaml\n";
		continue;
	}
	
	$result = post_request( $data['url'], $data );
	if ( ! $result ){
		unlink( $dir . '/lock' );
		die( 'POST error' );
		// TODO: log failure and/or retry later
	}
	
	rename( $dir, '.' . $dir );
	exec( "rm -r .{$dir}" );

}

// end of file process_triggers.php

