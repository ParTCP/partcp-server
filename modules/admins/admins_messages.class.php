<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Admins_Messages {

	static function handle_root_appointment( $message, $receipt ){
		global $Admins, $Config, $FileSystem, $Timestamp;
		$ptcpId = $message->get('Participant-Id');
		$secret = $message->get('Root-Appointment-Secret');
		if ( $secret ){
			$hash = $Config['root_appointment_secret'] ?? $Config['Root-Appointment-Secret'] ?? NULL;
			if ( ! $hash ){
				$receipt->set_rejection( 31, _('No root appointment secret provided on this server') );
				return $receipt->dump( TRUE );
			}
			if ( hash( 'sha256', $secret ) != $hash ){
				$receipt->set_rejection( 32, _('Invalid secret') );
				return $receipt->dump( TRUE );
			}
			if ( count( $Admins->get_list( 0 ) ) ){
				$receipt->set_rejection( 33, _('Secret may only be used when there are no root admins') );
				return $receipt->dump( TRUE );
			}
		}
		else {
			if ( count( $Admins->get_list( 0 ) )
				&& ! ptcp_is_authorized( $message->get('From'), 'root-appointment' )
			){
				$receipt->set_rejection( 34, _('Sender is not authorized to appoint new root admins') );
				return $receipt->dump( TRUE );
			}
		}
		$identity = new ParTCP_Public_Identity( $ptcpId, TRUE );
		if ( ! $identity->pubKey ){
			$receipt->set_rejection( 41, _('Participant does not exist') );
			return $receipt->dump( TRUE );
		}
		if ( ! $adminList = $Admins->appoint( $ptcpId, 0 ) ){
			$receipt->set_rejection( 42, _('Participant has already been appointed') );
			return $receipt->dump( TRUE );
		}
		$receipt->set( 'Message-Type', 'root-appointment-confirmation' );
		$receipt->set( 'Admins', $adminList );
		$receiptString = $receipt->dump( TRUE );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-root-apppointment';
		$FileSystem->put_contents( "root-admins/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_root_dismissal( $message, $receipt ){
		global $Admins, $Config, $FileSystem, $Timestamp;
		if ( ! ptcp_is_authorized( $message->get('From'), 'root-dismissal' ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to dismiss root admins') );
			return $receipt->dump( TRUE );
		}
		$adminList = $Admins->get_list( 0 );
		if ( count( $adminList ) <= 2 ){
			$receipt->set_rejection( 41, _('There are not enough root admins to dismiss one') );
			return $receipt->dump( TRUE );
		}
		$adminList = $Admins->dismiss( $message->get('Participant-Id'), 0 );
		if ( $adminList === FALSE ){
			$receipt->set_rejection( 42, _('Participant has not been appointed before') );
			return $receipt->dump( TRUE );
		}
		$receipt->set( 'Message-Type', 'root-dismissal-confirmation' );
		$receipt->set( 'Admins', $adminList );
		$receiptString = $receipt->dump( TRUE );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-root-dismissal';
		$FileSystem->put_contents( "root-admins/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_root_resignation( $message, $receipt ){
		global $Admins, $FileSystem, $Timestamp;
		// check if sender is appointed
		$sender = $message->get('From');
		if ( ! $Admins->is_appointed( $sender, 0 ) ){
			$receipt->set_rejection( 41, _('Sender is not appointed as root admin') );
			return $receipt->dump( TRUE );
		}
		// store and return the result
		$adminList = $Admins->dismiss( $sender, 0 );
		$receipt->set( 'Message-Type', 'root-resignation-confirmation' );
		$receipt->set( 'Admins', $adminList );
		$receiptString = $receipt->dump( TRUE );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-root-resignation';
		$FileSystem->put_contents( "root-admins/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_admin_appointment( $message, $receipt ){
		global $Admins, $Events, $Groups, $FileSystem, $ServerData, $Timestamp;
		$baseDir = '';
		$object = [ 'type' => 'server', 'id' => '', 'dir' => '/' ];
		// check if it's a group-based appointment
		$groupId = $message->get('Group-Id');
		$ptcpId = $message->get('Participant-Id');
		if ( $groupId && is_object( $Groups ) ){
			$baseDir = $Groups->get_dir( $groupId );
			$object = [ 'type' => 'group', 'id' => $groupId, 'dir' => $baseDir ];
			if ( ! $Groups->get_data( $groupId ) ){
				$receipt->set_rejection( 41, _('Group does not exist') );
				return $receipt->dump( TRUE );
			}
		}
		// check if it's an event-based appointment
		$eventId = $message->get('Event-Id');
		if ( $eventId && is_object( $Events )
		){
			$baseDir = $Events->get_dir( $eventId );
			$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $baseDir ];
			if ( ! $Events->get_data( $eventId ) ){
				$receipt->set_rejection( 42, _('Event does not exist') );
				return $receipt->dump( TRUE );
			}
		}
		// check if sender is authorized
		if ( ! ptcp_is_authorized( $message->get('From'), 'admin-appointment', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to appoint new admins') );
			return $receipt->dump( TRUE );
		}
		// check if participant exists
		$identity = new ParTCP_Public_Identity( $ptcpId, TRUE );
		if ( ! $identity->pubKey ){
			$receipt->set_rejection( 43, _('Participant does not exist') );
			return $receipt->dump( TRUE );
		}
		// appoint participant
		if ( ! $adminList = $Admins->appoint( $ptcpId, $baseDir ) ){
			$receipt->set_rejection( 44, _('Participant has already been appointed') );
			return $receipt->dump( TRUE );
		}
		// store and return the result
		$receipt->set( 'Message-Type', 'admin-appointment-confirmation' );
		$receipt->set( 'Admins', $adminList );
		$receiptString = $receipt->dump( TRUE );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-admin-apppointment';
		$dir = $baseDir ? "{$baseDir}/" : '';
		$FileSystem->put_contents( "{$dir}admins/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_admin_dismissal( $message, $receipt ){
		global $Admins, $Events, $Groups, $FileSystem, $Timestamp;
		$baseDir = '';
		$object = [ 'type' => 'server', 'id' => '', 'dir' => '/' ];
		// check if it's a group-based dismissal
		$groupId = $message->get('Group-Id');
		if ( $groupId && is_object( $Groups ) ){
			$baseDir = $Groups->get_dir( $groupId );
			$object = [ 'type' => 'group', 'id' => $groupId, 'dir' => $baseDir ];
			if ( ! $Groups->get_data( $groupId ) ){
				$receipt->set_rejection( 41, _('Group does not exist') );
				return $receipt->dump( TRUE );
			}
		}
		// check if it's an event-based dismissal
		$eventId = $message->get('Event-Id');
		if ( $eventId && is_object( $Events )
		){
			$baseDir = $Events->get_dir( $eventId );
			$object = [ 'type' => 'event', 'id' => $eventId, 'dir' => $baseDir ];
			if ( ! $Events->get_data( $eventId ) ){
				$receipt->set_rejection( 42, _('Event does not exist') );
				return $receipt->dump( TRUE );
			}
		}
		// check if sender is authorized
		if ( ! ptcp_is_authorized( $message->get('From'), 'admin-dismissal', $object ) ){
			$receipt->set_rejection( 31, _('Sender is not authorized to dismiss admins') );
			return $receipt->dump( TRUE );
		}
		// dismiss participant
		$adminList = $Admins->dismiss( $message->get('Participant-Id'), $baseDir );
		if ( $adminList === FALSE ){
			$receipt->set_rejection( 43, _('Participant has not been appointed before') );
			return $receipt->dump( TRUE );
		}
		// store and return the result
		$receipt->set( 'Message-Type', 'admin-dismissal-confirmation' );
		$receipt->set( 'Admins', $adminList );
		$receiptString = $receipt->dump( TRUE );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-admin-dismissal';
		$dir = $baseDir ? "{$baseDir}/" : '';
		$FileSystem->put_contents( "{$dir}admins/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_admin_resignation( $message, $receipt ){
		global $Admins, $Events, $Groups, $FileSystem, $Timestamp;
		$baseDir = '';
		// check if it's a group-based dismissal
		$groupId = $message->get('Group-Id');
		if ( $groupId && is_object( $Groups ) ){
			$baseDir = $Groups->get_dir( $groupId );
			if ( ! $Groups->get_data( $groupId ) ){
				$receipt->set_rejection( 41, _('Group does not exist') );
				return $receipt->dump( TRUE );
			}
		}
		// check if it's an event-based dismissal
		$eventId = $message->get('Event-Id');
		if ( $eventId && is_object( $Events )
		){
			$baseDir = $Events->get_dir( $eventId );
			if ( ! $Events->get_data( $eventId ) ){
				$receipt->set_rejection( 42, _('Event does not exist') );
				return $receipt->dump( TRUE );
			}
		}
		// check if sender is appointed
		$sender = $message->get('From');
		if ( ! $Admins->is_appointed( $sender, $baseDir ) ){
			$receipt->set_rejection( 43, _('Sender is not appointed as admin') );
			return $receipt->dump( TRUE );
		}
		// store and return the result
		$adminList = $Admins->dismiss( $sender, $baseDir );
		$receipt->set( 'Message-Type', 'admin-resignation-confirmation' );
		$receipt->set( 'Admins', $adminList );
		$receiptString = $receipt->dump( TRUE );
		$fileName = date( 'Ymd-His', $Timestamp ) . '-admin-resignation';
		$dir = $baseDir ? "{$baseDir}/" : '';
		$FileSystem->put_contents( "{$dir}admins/{$fileName}", $receiptString );
		return $receiptString;
	}


	static function handle_admin_list_request( $message, $receipt ){
		global $Admins, $Events, $Groups;
		$groupId = $message->get('Group-Id');
		$eventId = $message->get('Event-Id');
		if ( $groupId && $eventId ){
			$receipt->set_rejection( 22, _('Group and event ID must not be specified both') );
			return $receipt->dump( TRUE );
		}
		$baseDir = '';
		// check if it's a group-based appointment
		if ( $groupId && is_object( $Groups ) ){
			if ( ! $Groups->get_data( $groupId ) ){
				$receipt->set_rejection( 41, _('Group does not exist') );
				return $receipt->dump( TRUE );
			}
			$baseDir = $Groups->get_dir( $groupId );
			$receipt->set( 'Group-Id', $groupId );
		}
		// check if it's an event-based appointment
		if ( $eventId && is_object( $Events )
		){
			if ( ! $Events->get_data( $eventId ) ){
				$receipt->set_rejection( 42, _('Event does not exist') );
				return $receipt->dump( TRUE );
			}
			$baseDir = $Events->get_dir( $eventId );
			$receipt->set( 'Event-Id', $eventId );
		}
		// get list and return result
		$result = $Admins->get_responsibles( $baseDir );
		$receipt->set( 'Message-Type', 'admin-list' );
		$receipt->set( 'Admins', $result['list'] );
		$receipt->set( 'Direct-Appointment', $result['direct'] );
		return $receipt->dump( TRUE );
	}

}

// end of file lib/admin_messages.class.php

