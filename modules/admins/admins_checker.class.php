<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Admins_Checker {

	static function is_authorized( $ptcpId, $messageType, $object ){
		global $Admins;
		if ( empty( $object['dir'] ) ){
			return NULL;
		}
		$specialTypes = [ // message types where only parent admins are relevant
			'admin-appointment', 'admin-dismissal',
			'root-appointment', 'root-dismissal'
		];
		if ( ! in_array( $messageType, $specialTypes ) ){
			$adminList = $Admins->get_list( $object['dir'] );
			if ( $adminList ){
				return ptcp_is_in_list( $ptcpId, $adminList );
			}
		}
		// object admins not specified or irrelevant, so check parent admins
		$dir = $object['dir'];
		while ( ! in_array( $dir, [ '/', '.' ] ) ){
			$dir = dirname( $dir );
			$adminList = $Admins->get_list( $dir );
			if ( $adminList ){
				break;
			}
		}
		// if no parent admins are specified, check root admins
		if ( empty( $adminList ) ){
			$adminList = $Admins->get_list( 0 );
		}
		return $adminList && ptcp_is_in_list( $ptcpId, $adminList ) ? TRUE : NULL;
	}

}

// end of file lib/admins_checker.class.php

