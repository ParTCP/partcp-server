<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Admins {

	public $fileSystem;
	public $serverName;


	public function __construct( $fileSystem, $serverName ){
		$this->fileSystem = $fileSystem;
		$this->serverName = $serverName;
	}


	public function get_list( $baseDir = '' ){
		if ( $baseDir === 0 ){
			$dir = 'root-admins';
		}
		elseif ( ! $baseDir || $baseDir == '.' ){
			$dir = 'admins';
		}
		else {
			$dir = $baseDir . '/admins';
		}
		if ( ! $this->fileSystem->exists( $dir ) ){
			return [];
		}
		$data = $this->fileSystem->get_recent_contents( $dir, "[0-9]*-{admin,root}-*" );
		if ( ! $data ){
			return [];
		}
		$message = yaml_parse( $data );
		$list = $message['Admins'] ?? [];
		if ( $this->serverName ){
			$list = array_map( [ $this, 'sanitize_ptcp_id' ], $list );
		}
		return $list;
	}


	public function get_responsibles( $baseDir = '' ){
		$direct = TRUE;
		do {
			$list = $this->get_list( $baseDir );
			if ( $list || $baseDir == '' ){
				break;
			}
			$baseDir = dirname( $baseDir );
			$baseDir = $baseDir == '.' ? '' : $baseDir;
			$direct = FALSE;
		}
		while ( TRUE );
		return compact( 'list', 'direct' );
	}

	public function is_appointed( $ptcpId, $baseDir ){
		$adminList = $this->get_list( $baseDir );
		$ptcpId = $this->sanitize_ptcp_id( $ptcpId );
		return $adminList && ptcp_is_in_list( $ptcpId, $adminList );
	}


	public function appoint( $ptcpId, $baseDir ){
		$adminList = $this->get_list( $baseDir );
		$ptcpId = $this->sanitize_ptcp_id( $ptcpId );
		if ( ptcp_is_in_list( $ptcpId, $adminList ) ){
			return FALSE; // participant is already appointed as admin
		}
		$adminList[] = $ptcpId;
		sort( $adminList );
		return $adminList;
	}


	public function dismiss( $ptcpId, $baseDir ){
		$adminList = $this->get_list( $baseDir );
		$ptcpId = $this->sanitize_ptcp_id( $ptcpId );
		if ( ! ptcp_is_in_list( $ptcpId, $adminList ) ){
			return FALSE; // participant has not been appointed as admin
		}
		$adminList = array_diff( $adminList, [ $ptcpId ] );
		return array_values( $adminList );
	}


	private function sanitize_ptcp_id( $ptcpId ){
		if ( ! $this->serverName ){
			return $ptcpId;
		}
		return str_replace( "@{$this->serverName}", '', $ptcpId );
	}
}

// end of file lib/admins.class.php

