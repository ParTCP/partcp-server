<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class Mtd_Manager {

	public $dirList = [];
	public $lastError;


	public function __construct( $dirList = [] ){
		$this->dirList = $dirList;
	}


	public function register_dir( $path ){
		array_push( $this->dirList, $path );
	}


	public function get_mtd( $messageType ){
		$this->lastError = '';
		foreach ( $this->dirList as $dir ){
			$path = "{$dir}/{$messageType}.yaml";
			if ( file_exists( $path ) ){
				$found = $path;
				break;
			}
		}
		if ( empty( $found ) ){
			return FALSE;
		}
		try {
			$mtd = yaml_parse( file_get_contents( $found ) );
		}
		catch( Exception $e ) {
			$this->lastError = $e->getMessage();
			return FALSE;
		}
		if ( ! empty( $mtd['Inherits'] ) ){
			$inherited = $this->get_mtd( $mtd['Inherits'] );
			if ( ! $inherited ){
				return FALSE;
			}
			$mtd = array_replace_recursive( $inherited, $mtd );
			unset( $mtd['Inherits'] );
		}
		return $mtd;
	}


	public function get_mtd_list( $type = 'request' ){
		$this->lastError = '';
		$mtdList = [];
		foreach ( $this->dirList as $dir ){
			if ( ! chdir( $dir ) ){
				continue;
			}
			foreach ( glob('*.yaml') as $file ){
				try {
					$mtd = yaml_parse( file_get_contents( $file ) );
					if ( empty( $mtd['Name'] ) || empty( $mtd['Type'] )
						|| ( $type && $mtd['Type'] != $type )
					){
						continue;
					}
					$mtdList[] = $mtd['Name'];
				}
				catch( Exception $e ) {
					continue;
				}
			}
		}
		return $mtdList;
	}

}


// end of file mtd_manager.class.php

