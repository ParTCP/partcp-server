<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2023-24 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Server_Key_Storage extends ParTCP_Key_Storage_Fs {


	public static function store_pubkey( $id, $pubKey ){
		// do not store public keys
	}


	public static function get_pubkey( $id ){
		foreach ( $GLOBALS['IdProviders'] as $provider ){
			$info = $provider->get_info( $id );
			if ( $info ){
				return $info['public_key'] ?? FALSE;
			}
		}
		return FALSE;
	}

}


// end of file server_key_storage.class.php

