# ParTCP Server

ParTCP server is a platform for conducting democratic decision making
processes. This is beta software right now, so expect errors and missing
features.

Technical documentation (in German only) can be found [here][1].

[1]: https://partcp.codeberg.page


## System requirements

- Linux
- OpenSSL
- Apache2 with headers and rewrite module
- PHP 8 with gettext, intl, mbstring, openssl, sodium and yaml extension
- Git (for convenient installation and upgrading only)


## Installation (for testing environments only, not for production use!)

The following instructions assume that the root directory of your web server is
`/var/www/partcp.my-community.org`, that the address is
`partcp.my-community.org` and that the web server process is running as user
`www-data`.

1. **Clone project to your web server (as root)**

    ```sh
    $ cd /var/www
    $ sudo git clone --recurse-submodules https://codeberg.org/ParTCP/partcp-server.git
    $ sudo mv partcp-server partcp.my-community.org
    ```

2. **Make all files and directories accessible for all users**

    ```sh
    $ cd partcp.my-community.org
    $ sudo chown -R www-data .
    $ sudo find . -type f -exec chmod 644 {} \;
    $ sudo find . -type d -exec chmod 755 {} \;
    ```

3. **Create a directory for storing server keys**

	Please note: If you have multiple server instances on the same machine (e.g.
	for testing or development), each one needs its own key directory.

    ```sh
    $ sudo mkdir .keys
    $ sudo chown www-data .keys
    $ sudo chmod 700 .keys
    ```

4. **Create directories for data storage**

    ```sh
    $ sudo mkdir data worm
    $ sudo chown www-data data worm
    $ sudo chmod 755 data worm
    ```

5. **Adapt Apache configuration for ParTCP root directory**

   ```sh
   $ sudo nano /etc/apache2/site-available/partcp.my-community.org.conf
   ```

   ```apache
   <Directory "/var/www/partcp.my-community.org">
      # Make resources accessible regardless of client domain
      Header set Access-Control-Allow-Origin "*"
      Header set Access-Control-Allow-Methods "GET, POST, OPTIONS"
      Header set Access-Control-Allow-Headers "Accept, Authorization, Access-Control-Allow-Origin"
      Header append Access-Control-Allow-Headers "Content-Type, Content-Length, Cookie, Origin"
      Header append Access-Control-Allow-Headers "User-Agent, X-Partcp-Version, X-Partcp-Kx-Method"
   </Directory>
   ```

6. **Reload Apache configuration**

   ```sh
   $ sudo systemctl reload apache2
   ```

7. **Enable HTTPS for the new domain**

	To encrypt the connection to your domain, you have to install an SSL certificate. An easy way
	to accomplish this is via [Let's Encrypt](https://letsencrypt.org/getting-started).

8. **Create and edit configuration file**

    ```sh
    $ sudo cp config-example.php config.php
    $ sudo nano config.php
    ```

	Adapt configuration parameters to your needs and finish editing by
	pressing `<ctrl-o>`, `<return>`, `<ctrl-x>`.


9. **Send test message**

	Check if the server works properly by sending a "ping" message
	from your local machine:

    ```sh
    $ curl -d "Message-Type: ping" https://partcp.my-community.org
    ```

   You should receive a response like this:

    ```yaml
    Signature: WHM08/cytInHm2h4z64J1pUjNg2U88B1q1ovZA5ETxSkJYhJQuKjn2RoA9X6k5Y2HCOTrrVcfRpk6/agk8trAg==
    Message-Type: echo
    To: <anonymous>
    From: partcp.my-community.org
    Date: 2021-03-30T09:06:56+02:00
    Original-Message: 'Message-Type: ping'
    Verification-Result: missing signature, nothing to verify
    Public-Key: >
      8BbYgAcG4yPfKKhh64+uo/AxV0NkcxhIAmnnHq1aAosBJQqBJk1Fy6Hj3g6vsZxOv4W/gN4NwzLTIGerDMNxHg==
    ```


## Setting up a root account

1. **Install ParTCP command line client on your local machine.**

    https://codeberg.org/ParTCP/partcp-cli


2. **Create local identity.**

    ```sh
    $ partcp create_id root@partcp.my-community.org
    ```

3. **Create random string as credential for key submission.**

   ```sh
   $ export credential=`openssl rand -base64 12`
   ```

4. **Generate credential hash code.**

    ```sh
    $ echo -n $credential | sha256sum
    ```

    Copy output to clipboard (without spaces and dash at the end).


5. **Register participant on the server.**

	Compose `From` and `Participant-Id` elements from identity created in step 2.

    ```sh
    $ partcp send
    Signature: null
    To: partcp.my-community.org
    From: root@partcp.my-community.org
    Message-Type: root-registration
    Participant-Id: root
    Credential: <insert clipboard content here>
    <ctrl-D>
    ```

6. **Submit the public key of the new identity to the server.**

    ```sh
    $ partcp submit_key root@partcp.my-community.org $credential
    ```


7. **Appoint participant as root admin.**

    ```sh
    $ partcp send
    To: partcp.my-community.org
    From: root@partcp.my-community.org
    Message-Type: root-appointment
    Participant-Id: root
    <ctrl-D>
    ```

