<?php

$Config['path_to_data'] = '/var/www/partcp.my-community.org/data';
$Config['path_to_worm'] = '/var/www/partcp.my-community.org/worm';
$Config['path_to_keys'] = '/var/www/partcp.my-community.org/.keys';
$Config['estimated_turnout'] = 5000;
$Config['max_items_per_dir'] = 9999;
$Config['server_data'] = [
    'community' => 'My ParTCP Community',
    'name' => 'partcp.my-community.org',
    'modules' => [ 'server', 'admins', 'local_id', 'groups', 'events', 'core' ]
];

date_default_timezone_set('Europe/Berlin');

