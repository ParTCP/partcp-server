<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

if ( file_get_contents('php://input') ){
        include 'process.php';
        exit;
}

include 'initialize.php';
$Server = new ParTCP_Server( $FileSystem );
$ServerData = $Server->get_data() ?: $ServerData;

?><!doctype html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $ServerData['community'] ?? $ServerData['name'] ?></title>
	<style>
		body {
			background-color: #ccc;
		}
		.main {
			box-sizing: border-box;
			width: 90%;
			max-width: 60em;
			margin: 3em auto;
			padding: 3em;
			background-color: white;
			border: 1px solid #999;
		}
		.main h1 {
			margin-top: 0;
		}
		.privacy {
			border-top: 1px solid #999;
			margin-top: 2em;
			font-size: 80%;
			opacity: 70%;
	</style>
</head>
<body>
	<div class="main">
		<h1><?= $ServerData['community'] ?? $ServerData['name'] ?></h1>
		<?php if ( empty( $ServerData['description'] ) ): ?>
			<p>
				<?= sprintf( _('This is a %s powered server.'), '<a href="https://partcp.org">ParTCP</a>' ) ?>
				<?= _('Please use a ParTCP client app to use its services.') ?>
			</p>
		<?php else: ?>
			<p><?= $ServerData['description'] ?></p>
		<?php endif; ?>
		<?php if ( ! empty( $ServerData['privacy_notice'] ) ): ?>
			<div class="privacy">
				<h4><?= _('Privacy Notice') ?></h4>
				<p><?= $ServerData['privacy_notice'] ?? '' ?></p>
			</div>
		<?php endif; ?>
	</div>
</body>
</html>

